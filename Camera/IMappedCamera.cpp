/* 
 * File:   IMappedCamera.cpp
 * Author: Kevin
 * 
 * Created on 29 March 2014, 11:35
 */

#include "IMappedCamera.h"
#include <stdexcept>
#include <sstream>

using namespace Candela;
using namespace Candela::Camera;

IMappedCamera::IMappedCamera(const Mathematics::Vector& direction, 
        const CameraPlane& cp, Aperture aperture, const Mathematics::Rectangle_I& pixelRect)
        : ICamera(direction, cp, aperture), pixelRect_ ( pixelRect )
{
    setRatio();
}

void IMappedCamera::setPixelRect(const Mathematics::Rectangle_I& pixelRect) {
    pixelRect_ = pixelRect;
    setRatio();
}

void IMappedCamera::setRatio() {
    filmToPixelRatioX_ = filmPlane_.rect.getWidth() / pixelRect_.getWidth();
    filmToPixelRatioY_ = -filmPlane_.rect.getHeight() / pixelRect_.getHeight();
}

//overridden
void IMappedCamera::setFilmPlane(const CameraPlane& cp) {
    ICamera::setFilmPlane(cp);
    setRatio();
}

void IMappedCamera::getRayPixel(int x, int y, Mathematics::BoundedRay& ray) const {
    getRay(filmPlane_.rect.left + (x + 0.5f) * filmToPixelRatioX_,
           filmPlane_.rect.top  + (y + 0.5f) * filmToPixelRatioY_, ray);
}


void IMappedCamera::getRayPixel(int x, int y, float pixelVarX, float pixelVarY, Mathematics::BoundedRay& ray) const {
    //const CameraPlane& objPlane = getObjectPlane();
    getRay(filmPlane_.rect.left + (x + 0.5f) * filmToPixelRatioX_, 
           filmPlane_.rect.top  + (y + 0.5f) * filmToPixelRatioY_, 
           (pixelVarX - 0.5f) * filmToPixelRatioX_, 
           (pixelVarY - 0.5f) * filmToPixelRatioY_, ray);
}

void IMappedCamera::getRayPixel(int x, int y, float pixelVarX, float pixelVarY, float apertureVarX, float apertureVarY, Mathematics::BoundedRay& ray) const {
    if(aperture_ == ICamera::Infinitesimal){
        getRayPixel(x, y, pixelVarX, pixelVarY, ray);
    }else
    {
        if(apertureSize_ == 0.f)
        {
            if(apertureVarX == 0.f && apertureVarY == 0.f)
            {
                //be graceful
                getRayPixel(x, y, pixelVarX, pixelVarY, ray);
            }else
            {
                //something is wrong, throw exception
                throw std::runtime_error("Aperture is enabled, but aperture size is zero and aperture variations other than zero have been inputted.");
            }
        }else
        {
            //calculate proper stuff
            getRay(filmPlane_.rect.left + (x + 0.5f) * filmToPixelRatioX_, 
                   filmPlane_.rect.top +  (y + 0.5f) * filmToPixelRatioY_, 
                   (pixelVarX - 0.5f) * filmToPixelRatioX_, 
                   (pixelVarY - 0.5f) * filmToPixelRatioY_, 
                    apertureSize_ * (apertureVarX - 0.5f), 
                    apertureSize_ * (apertureVarY - 0.5f), ray);
        }
    }
}

std::string IMappedCamera::toString() const {
    using namespace std;
    ostringstream sstr; //use own buffer to pre-allocate/reserve?
    sstr << ICamera::toString()
         << "\n\tIMappedCamera: " << this
         << "\n\tPixel Rectangle: " << pixelRect_
         << "\n\tFilm to Pixel Ratio = (" << filmToPixelRatioX_ << ", " << filmToPixelRatioY_
            << ")"
         ;
    return sstr.str();
}

const Mathematics::Rectangle_I& IMappedCamera::getPixelRect() const {
    return pixelRect_;
}



