/* 
 * File:   PinholeCamera.cpp
 * Author: Kevin
 * 
 * Created on 25 March 2014, 21:12
 */

#include "Camera/PinholeCamera.h"
#include "Mathematics/Float.h"
#include <sstream>

using namespace Candela::Camera;

PinholeCamera::PinholeCamera(const Mathematics::Vector& direction, 
                             const CameraPlane& cp, const Mathematics::Rectangle_I& pixelRect)
        : IMappedCamera(direction, cp, ICamera::Infinitesimal, pixelRect)
{
    changeListener();
}

PinholeCamera::PinholeCamera(const Mathematics::Vector& direction, const CameraPlane& cp, 
        const Mathematics::Rectangle_I& pixelRect, ICamera::Aperture aperture) 
        : IMappedCamera(direction, cp, aperture, pixelRect)
{
    changeListener();
}


void PinholeCamera::changeListener() {
    IMappedCamera::changeListener();
    objectPlaneAperture_ = position_ + w_ * filmPlane_.distance;
}

void PinholeCamera::getRay(float x, float y, Mathematics::BoundedRay& ray) const {
    using namespace Mathematics;
    Vector target = objectPlaneAperture_ + u_ * x + v_ * y;
    ray.setRay(position_, target - position_);
    ray.setMin(0.f); ray.setMax(Float::Infinity);
}

void PinholeCamera::getRay(float x, float y, float pixelVarX, float pixelVarY, Mathematics::BoundedRay& ray) const {
    using namespace Mathematics;
    Vector target = objectPlaneAperture_ + u_ * (x + pixelVarX) + v_ * (y + pixelVarY);
    ray.setRay(position_, target - position_);
    ray.setMin(0.f); ray.setMax(Float::Infinity);
}

void PinholeCamera::getRay(float x, float y, float pixelVarX, float pixelVarY, float apertureVarX, float apertureVarY, Mathematics::BoundedRay& ray) const {
    getRay(x, y, pixelVarX, pixelVarY, ray);
}


std::string PinholeCamera::toString() const {
    using namespace std;
    ostringstream sstr; //use own buffer to pre-allocate/reserve?
    sstr << IMappedCamera::toString()
         << "\n\tIdeal Pinhole Camera: " << this
         << "\n\tObject Plane Corner: " << objectPlaneAperture_
         ;
    return sstr.str();
}


