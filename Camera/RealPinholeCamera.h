/* 
 * File:   RealPinholeCamera.h
 * Author: Kevin
 *
 * Created on 30 March 2014, 15:07
 */

#ifndef REALPINHOLECAMERA_H
#define	REALPINHOLECAMERA_H

#include "PinholeCamera.h"

namespace Candela
{
    namespace Camera
    {

        class RealPinholeCamera 
                : public PinholeCamera
        {
        public:
            RealPinholeCamera(const Mathematics::Vector& direction, 
                            const CameraPlane& cp, const Mathematics::Rectangle_I& pixelRect);
            
            virtual void getRay(float x, float y, float pixelVarX, float pixelVarY, 
                                float apertureVarX, float apertureVarY, Mathematics::BoundedRay& ray) const;
            
            virtual std::string toString() const;
        private:

        };

    }
}

#endif	/* REALPINHOLECAMERA_H */

