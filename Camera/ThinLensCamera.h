/* 
 * File:   ThinLensCamera.h
 * Author: Kevin
 *
 * Created on 26 March 2014, 21:38
 */

#ifndef THINLENSCAMERA_H
#define	THINLENSCAMERA_H

#include "IMappedCamera.h"

namespace Candela
{
    namespace Camera
    {
        class ThinLensCamera 
                : public IMappedCamera
        {
        public:
            ThinLensCamera(const Mathematics::Vector& direction, 
                    const CameraPlane& cp, const Mathematics::Rectangle_I& pixelRect);
            
            void setFocalLength(float focalLength);
            void setFNumber(float fNumber);
            void setApertureSize(float apertureSize);
            void setObjectPlaneDistance(float distance);
            
            virtual void changeListener();
            
            virtual void getRay(float x, float y, Mathematics::BoundedRay& ray) const;
            virtual void getRay(float x, float y, float pixelVarX, float pixelVarY, Mathematics::BoundedRay& ray) const;
            virtual void getRay(float x, float y, float pixelVarX, float pixelVarY, 
                                float apertureVarX, float apertureVarY, Mathematics::BoundedRay& ray) const;
            
            /*override integer getRay methods to obtain more precision if neccessary*/
            
            virtual std::string toString() const;
            
        private:
            
            void update();
            
            CameraPlane objectPlane_;
            Mathematics::Vector objectPlaneAperture_;
            float focalLength_;
            float fNumber_;
            float magnification_;
        }; 
    }
}


#endif	/* THINLENSCAMERA_H */

