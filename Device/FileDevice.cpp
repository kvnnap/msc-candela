//
// Created by kvnna on 25/06/2016.
//

#include <cstdio>

#include "Mathematics/Vector3.h"
#include "Spectrum/RgbSpectrum.h"
#include "FileDevice.h"

using namespace std;
using namespace Candela::Device;
using namespace Candela::Spectrum;
using namespace Candela::Renderer;

FileDevice::FileDevice(const string& fileName, FileType p_fileType, const string& directoryPath, bool writeToSeqOfFiles)
    : fileName (fileName),
      directoryPath ( directoryPath ),
      seqNum (0),
      fileType(p_fileType),
      writeToSeqOfFiles ( writeToSeqOfFiles )
{ }

void FileDevice::writePPM(const string& filePath, const RadianceBuffer &radianceBuffer,
                          RadianceContent::RadianceType radianceType) {
    // Open File
    FILE *fp = fopen(filePath.c_str(), "wb"); /* b - binary mode */
    if (fp == nullptr) {
        throw runtime_error("Could not open file: " + filePath);
    }
    fprintf(fp, "P6\n%d %d\n255\n", radianceBuffer.getWidth(), radianceBuffer.getHeight());
    for (size_t y = 0; y < radianceBuffer.getHeight(); ++y) {
        for (size_t x = 0; x < radianceBuffer.getWidth(); ++x) {
            uint8_t color[3];
            RgbSpectrum rgbSpectrum = radianceType == RadianceContent::FINAL ? radianceBuffer(x, y).final.toTristimulusSRGB() :
                                      radianceType == RadianceContent::DIRECT ? radianceBuffer(x, y).direct.toTristimulusSRGB() :
                                      radianceBuffer(x, y).indirect.toTristimulusSRGB();
            //rgbSpectrum.alphaCorrect();
            rgbSpectrum.clamp();
            const Mathematics::Vector& rgb = rgbSpectrum.getAbcReference();
            color[0] = static_cast<uint8_t>(rgb.x * 255);
            color[1] = static_cast<uint8_t>(rgb.y * 255);
            color[2] = static_cast<uint8_t>(rgb.z * 255);
            fwrite(color, 1, 3, fp);
        }
    }
    fclose(fp);
}

void FileDevice::write(const RadianceBuffer &radianceBuffer,
                       RadianceContent::RadianceType radianceType) {
    // Check file name
    if (fileName.length() == 0) {
        return;
    }
    // Construct file path
    string constructedPath = (directoryPath.length() == 0 ? "." : directoryPath) + "/" + fileName;
    if (writeToSeqOfFiles) {
        constructedPath += "-";
        constructedPath += to_string(++seqNum);
    }
    switch (fileType) {
        case PPM:
            constructedPath += ".ppm";
            writePPM(constructedPath, radianceBuffer, radianceType);
        //default:
    }
}

void FileDevice::setWriteToSequenceOfFiles(bool p_writeToSeqOfFiles) {
    writeToSeqOfFiles = p_writeToSeqOfFiles;
}

void FileDevice::setDirectoryPath(const std::string &p_filePath) {
    directoryPath = p_filePath;
}

void FileDevice::resetSeqNum() {
    seqNum = 0;
}

void FileDevice::setFileType(FileType p_fileType) {
    fileType = p_fileType;
}


void FileDevice::setFileName(const std::string &p_fileName) {
    fileName = p_fileName;
}

