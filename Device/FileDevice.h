//
// Created by kvnna on 25/06/2016.
//

#ifndef MSC_CANDELA_FILEDEVICE_H
#define MSC_CANDELA_FILEDEVICE_H

#include <atomic>
#include "IDevice.h"
#include "Renderer/RadianceBuffer.h"

namespace Candela
{
    namespace Device
    {
        class FileDevice
            : public IDevice
        {
            enum FileType : uint8_t {
                PPM = 0
            };

        public:
            FileDevice(const std::string& fileName = "", FileType p_fileType = PPM, const std::string& p_directoryPath = ".", bool p_writeToSeqOfFiles = false);
            void setWriteToSequenceOfFiles(bool p_writeToSeqOfFiles);
            void setDirectoryPath(const std::string &p_directoryPath);
            void setFileName(const std::string &p_fileName);
            void setFileType(FileType p_fileType);
            void resetSeqNum();
            void writePPM(const std::string& filePath, const Renderer::RadianceBuffer& radianceBuffer,
                          Renderer::RadianceContent::RadianceType radianceType);
            // overridden virtuals
            void write(const Renderer::RadianceBuffer& radianceBuffer, Renderer::RadianceContent::RadianceType radianceType) override;

        private:
            std::string fileName;
            std::string directoryPath;
            std::atomic<size_t> seqNum;
            FileType fileType;
            bool writeToSeqOfFiles;

        };
    }
}


#endif //MSC_CANDELA_FILEDEVICE_H
