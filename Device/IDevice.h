//
// Created by kvnna on 25/06/2016.
//

#ifndef MSC_CANDELA_IDEVICE_H
#define MSC_CANDELA_IDEVICE_H

#include "Renderer/RadianceBuffer.h"

namespace Candela
{
    namespace Device
    {
        class IDevice {
        public:
            virtual ~IDevice();

            // Write the buffer
            virtual void write(const Renderer::RadianceBuffer& radianceBuffer, Renderer::RadianceContent::RadianceType radianceType) = 0;
        };
    }
}

#endif //MSC_CANDELA_IDEVICE_H
