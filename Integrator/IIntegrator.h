//
// Created by kvnna on 25/06/2016.
//

#ifndef MSC_CANDELA_IINTEGRATOR_H
#define MSC_CANDELA_IINTEGRATOR_H

#include <Renderer/RadianceBuffer.h>
#include <Scene/Scene.h>

namespace Candela
{
    // Integrators give back the radiance value from an intersection point
    namespace Integrator
    {
        class IIntegrator {
        public:
            virtual ~IIntegrator();
            virtual void radiance(Renderer::RadianceContent& radianceContent,
                                  const Scene::Scene& scene,
                                  const Mathematics::BoundedRay& ray) = 0;
        };
    }
}

#endif //MSC_CANDELA_IINTEGRATOR_H
