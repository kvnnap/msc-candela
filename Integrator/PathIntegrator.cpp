//
// Created by kvnna on 28/06/2016.
//

#include <memory>
#include <iostream>

#include <Material/MatteMaterial.h>
#include <Mathematics/Float.h>
#include <Mathematics/Funtions.h>
#include "SimpleIntegrator.h"
#include "Spectrum/RgbSpectrum.h"
#include "PathIntegrator.h"

using namespace std;
using namespace Candela::Integrator;
using namespace Candela::Sampler;
using namespace Candela::Spectrum;
using namespace Candela::Integrator;
using namespace Candela::Mathematics;
using namespace Candela::Material;
using namespace Candela::Scene;
using namespace Candela::Light;
using namespace Candela::Renderer;

PathIntegrator::PathIntegrator()
        : maxDepth (1)
{

}

void PathIntegrator::radiance(RadianceContent &radianceContent, const Scene::Scene &scene,
                              const BoundedRay &primaryRay)
{
    radianceContent.final = radianceContent.direct = radianceContent.indirect = 0.f;

    bool notReady = true;
    PathContext pathContext;
    pathContext.ray = primaryRay;

    for (size_t depth = 0; notReady; ++depth) {
        pathContext.intersection = intersect(scene, pathContext.ray);
        if (pathContext.intersection.shapeIntersection.Type == Shape::Intersection::External) {

            // Compute intersection point
            pathContext.intersection.shapeIntersection.point = pathContext.ray.getPoint(
                    pathContext.intersection.shapeIntersection.Distance
            );
            const Vector& intersectionPoint = pathContext.intersection.shapeIntersection.point;

            //  Fire random ray on the hemisphere
            BoundedRay randomRay(intersectionPoint, randomRayHemisphere(
                    intersectionPoint,
                    pathContext.intersection.shapeIntersection.Intersectant->getUnitNormal(
                            pathContext.intersection.shapeIntersection
                    ),
                    sampler)
            );
            randomRay.setMin(100.f * Float::MachineEpsilon);

            // Compute Diffuse
            float dotDiffuse = pathContext.intersection.shapeIntersection.Intersectant->getUnitNormal(
                    pathContext.intersection.shapeIntersection
            ) * /*~*/randomRay.getDirection();

            // Check shadow ray probability
            size_t lightIndex = sampler.chooseInRange(0, scene.getLights().size() - 1);
            const unique_ptr<ILight>& light = scene.getLights()[lightIndex];
            Vector shadRayDir = light->getCentrePoint() - intersectionPoint;

            float dotShadow = pathContext.intersection.shapeIntersection.Intersectant->getUnitNormal(
                    pathContext.intersection.shapeIntersection
            ) * ~shadRayDir;

            dotShadow = Functions::clamp(dotShadow, 0.f, 1.f);

            float termination = 1.f / (maxDepth - depth);
            float probabilityDiffuse = (dotDiffuse / (dotDiffuse + dotShadow)) * (1.f - termination);
            float probabilityLight = (dotShadow / (dotDiffuse + dotShadow)) * (1.f - termination);
            float rand = sampler.nextSample();

            {
                BoundedRay shadowRay(intersectionPoint,
                                     shadRayDir);
                shadowRay.setMin(100.f * Float::MachineEpsilon);
                shadowRay.setMax(1);
                if (intersect(scene, shadowRay).shapeIntersection.Type != Shape::Intersection::None) {
                    probabilityLight = 0.f;
                    probabilityDiffuse = 1.f - termination;
                }
            }

            // Add to stack
            if (rand < probabilityDiffuse) {

                // We will follow this ray,
                pathContext.coefficient = // dot / probabilityDiffuse * <--- this is one anyway
                        pathContext.intersection.primitive->getMaterial()->getDiffuseTexture().GetSpectrum(
                                pathContext.intersection.shapeIntersection.Intersectant->getTextureUv(
                                        pathContext.intersection.shapeIntersection
                                )
                        )->toTristimulusSRGB();
                pathContext.coefficient *= (dotDiffuse / probabilityDiffuse);
            } else {

                // Terminate here at light source
                directLighting(scene, pathContext, pathContext.coefficient);
                pathContext.coefficient /= (probabilityLight + termination);

                // Add to stack and exit out of loop
                notReady = false;
            }

            intersectionStack.push(pathContext);
            pathContext.ray = randomRay;
        } else {
            // We missed
            pathContext.coefficient = 0.f;
            intersectionStack.push(pathContext);
            notReady = false;
        }
    }

    // Caclulate the value
    radianceContent.final = intersectionStack.empty() ? 0.f : 1.f;
    while(!intersectionStack.empty()) {
        radianceContent.final *= intersectionStack.top().coefficient;
        intersectionStack.pop();
    }
}

Vector PathIntegrator::randomRayHemisphere(const Vector &intersectionPoint, const Vector &unitNormal, UniformSampler &sampler)
{
    // Generate basis
    // A vector perpendicular to normal
    Vector u;
    if (unitNormal.y != 0.f || unitNormal.x != 0.f) {
        u = ~Vector(unitNormal.y, -unitNormal.x, 0.f);
    } else {
        u = ~Vector(unitNormal.z, 0.f, -unitNormal.x);
    }
    // Another Perpendicular to both u and unitNormal
    Vector v = ~(u ^ unitNormal);
    const Vector& w = /*~(v ^ u);//*/unitNormal;

    // Generate two random numbers
    const float sample1 = sampler.nextSample(-1.f, 1.f);
    const float res = Functions::sqrt(1 - sample1 * sample1);
    Vector x = u * sample1  + v * sampler.nextSample(-res, res);

    // Find the constant that we need to multiply by n and add to x
    const float wDotx = w * x;
    float wC = Functions::sqrt(1 + wDotx * wDotx - x * x) - wDotx;

    x += wC * w;
    return x;
}

Intersection PathIntegrator::intersect(const Scene::Scene &scene, BoundedRay &ray) {
    // Intersect ray into scene
    Intersection intersection;
    if (scene.getMesh()->intersect(ray, intersection.shapeIntersection)) {
        intersection.primitive = static_cast<const Primitive*>(intersection.shapeIntersection.Intersectant);
    }
    return intersection;
}

void PathIntegrator::directLighting(const Scene::Scene &scene, const PathContext &pathContext, RgbSpectrum& outRgb) {
    //
    const Vector& intersectionPoint = pathContext.intersection.shapeIntersection.point;

    // Sample direct lighting - choose one light source at random for the time being and sample it
    size_t lightIndex = sampler.chooseInRange(0, scene.getLights().size() - 1);
    const unique_ptr<ILight>& light = scene.getLights()[lightIndex];

    // Cast ray towards light source to detect occluder, if any
    BoundedRay shadowRay(intersectionPoint,
                         light->getCentrePoint() - intersectionPoint);
    shadowRay.setMin(100.f * Float::MachineEpsilon);
    shadowRay.setMax(1);

    // Cast it
    Intersection shadowIntersection;// = intersect(scene, shadowRay);
    bool unoccluded = !scene.getMesh()->intersect(shadowRay, shadowIntersection.shapeIntersection);

    //
    if (unoccluded) {
        // dot product
        float dot = pathContext.intersection.shapeIntersection.Intersectant->getUnitNormal(
                pathContext.intersection.shapeIntersection
        )
                    * (~shadowRay.getDirection());
        if (dot >= 0) {
            light->radiance(shadowRay, outRgb);
            outRgb *= pathContext.intersection.primitive->getMaterial()->getDiffuseTexture().GetSpectrum(
                    pathContext.intersection.shapeIntersection.Intersectant->getTextureUv(pathContext.intersection.shapeIntersection)
            )->toTristimulusSRGB();
            outRgb *= dot;
            return;
        }
    }

    outRgb = 0.f;
}

void PathIntegrator::setRayMaxDepth(size_t p_maxDepth) {
    maxDepth = p_maxDepth;
}

size_t PathIntegrator::getRayMaxDepth() const {
    return maxDepth;
}




