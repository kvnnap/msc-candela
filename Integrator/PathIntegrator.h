//
// Created by kvnna on 28/06/2016.
//

#ifndef MSC_CANDELA_PATHINTEGRATOR_H
#define MSC_CANDELA_PATHINTEGRATOR_H

#include <stack>
#include "IIntegrator.h"
#include "Sampler/UniformSampler.h"

namespace Candela
{
    // Integrators give back the radiance value from an intersection point
    namespace Integrator
    {

        struct PathContext {
            Scene::Intersection intersection;
            Mathematics::BoundedRay ray;
            Spectrum::RgbSpectrum coefficient;
        };

        class PathIntegrator
            : public IIntegrator
        {
        public:
            PathIntegrator();
            void radiance(Renderer::RadianceContent& radianceContent,
                          const Scene::Scene& scene,
                          const Mathematics::BoundedRay& primaryRay) override;

            void setRayMaxDepth(size_t p_maxDepth);
            size_t getRayMaxDepth() const;

        private:

            Scene::Intersection intersect(const Scene::Scene& scene, Mathematics::BoundedRay& ray);
            void directLighting(const Scene::Scene& scene, const PathContext& pathContext, Spectrum::RgbSpectrum& outRgb);

            static Mathematics::Vector randomRayHemisphere(
                    const Mathematics::Vector& intersectionPoint,
                    const Mathematics::Vector& unitNormal,
                    Sampler::UniformSampler& sampler);

            Sampler::UniformSampler sampler;
            std::stack<PathContext> intersectionStack;
            size_t maxDepth;
        };

    }
}


#endif //MSC_CANDELA_PATHINTEGRATOR_H
