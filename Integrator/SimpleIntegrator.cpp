//
// Created by kvnna on 26/06/2016.
//
#include <memory>
#include <Material/MatteMaterial.h>
#include <Mathematics/Float.h>
#include "SimpleIntegrator.h"
#include "Spectrum/RgbSpectrum.h"

using namespace std;
using namespace Candela::Integrator;
using namespace Candela::Light;
using namespace Candela::Scene;
using namespace Candela::Spectrum;
using namespace Candela::Mathematics;
using namespace Candela::Material;

void SimpleIntegrator::radiance(Candela::Renderer::RadianceContent &radianceContent,
                                const Candela::Scene::Scene &scene, const Candela::Mathematics::BoundedRay &primaryRay) {

    Intersection intersection, shadowIntersection;
    BoundedRay pRay = primaryRay;

    // Intersect ray into scene
//    for (const unique_ptr<Primitive>& primitive : scene.getPrimitives()) {
//        if(primitive->intersect(pRay, intersection)) {
//            pRay.setMax(intersection.shapeIntersection.Distance);
//        }
//    }

    scene.getMesh()->intersect(pRay, intersection.shapeIntersection);

    //check if any intersections
    if(intersection.shapeIntersection.Type == Shape::Intersection::External)
    {
        intersection.primitive = static_cast<const Primitive*>(intersection.shapeIntersection.Intersectant);

        intersection.shapeIntersection.point = pRay.getPoint(intersection.shapeIntersection.Distance);
        const Vector& intersectionPoint = intersection.shapeIntersection.point;
        for (const unique_ptr<ILight>& light : scene.getLights()){

            // Cast ray towards light source to detect occluder, if any
            BoundedRay shadowRay (intersectionPoint,
                                  light->getCentrePoint() - intersectionPoint);
            shadowRay.setMin(1000.f * Float::MachineEpsilon);
            shadowRay.setMax(1);

            // Initialise radiance
            radianceContent.final = 0.f;

            // Cast it
            bool unoccluded = !scene.getMesh()->intersect(shadowRay, shadowIntersection.shapeIntersection);

            //
            if (unoccluded){
                // dot product
                float dot = intersection.shapeIntersection.Intersectant->getUnitNormal(intersection.shapeIntersection)
                             * (~shadowRay.getDirection());
                if (dot < 0) {
                    continue;
                }
                RgbSpectrum lightRgb;
                light->radiance(shadowRay, lightRgb);
                lightRgb *= intersection.primitive->getMaterial()->getDiffuseTexture().GetSpectrum(
                        intersection.shapeIntersection.Intersectant->getTextureUv(intersection.shapeIntersection)
                )->toTristimulusSRGB();
                lightRgb *= dot;

                radianceContent.final += lightRgb;
            }
            //radianceContent.final += intersection.primitive->getMaterial()->getDiffuseTexture().GetSpectrum({})->toTristimulusSRGB();
        }

    }
}

