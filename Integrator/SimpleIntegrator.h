//
// Created by kvnna on 26/06/2016.
//

#ifndef MSC_CANDELA_SIMPLEINTEGRATOR_H
#define MSC_CANDELA_SIMPLEINTEGRATOR_H

#include "IIntegrator.h"

namespace Candela {
    // Integrators give back the radiance value from an intersection point
    namespace Integrator {
        class SimpleIntegrator
            : public IIntegrator
        {
        public:
            void radiance(Renderer::RadianceContent& radianceContent,
                          const Scene::Scene& scene,
                          const Mathematics::BoundedRay& ray) override;

        private:

        };
    }
}


#endif //MSC_CANDELA_SIMPLEINTEGRATOR_H
