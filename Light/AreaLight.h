//
// Created by kvnna on 29/06/2016.
//

#ifndef MSC_CANDELA_AREALIGHT_H
#define MSC_CANDELA_AREALIGHT_H

#include "ILight.h"
#include "Spectrum/Spectrum.h"

namespace Candela
{
    namespace Light
    {
        class AreaLight
                : public ILight{
        public:
            AreaLight(const Mathematics::Vector& point, std::unique_ptr<Spectrum::ISpectrum> spectrum);

            float power() const override;
            void radiance(const Mathematics::Ray& ray, Spectrum::RgbSpectrum& a_spectrum) const override;
            const Mathematics::Vector& getCentrePoint() const override;

        private:
            Spectrum::RgbSpectrum emission;
        };
    }
}

#endif //MSC_CANDELA_AREALIGHT_H
