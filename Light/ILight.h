/* 
 * File:   ILight.h
 * Author: Kevin
 *
 * Created on August 24, 2014, 11:40 AM
 */

#ifndef ILIGHT_H
#define	ILIGHT_H

#include "Spectrum/RgbSpectrum.h"
#include "Mathematics/BoundedRay.h"

namespace Candela
{
    namespace Light
    {
        /**
         * Defines an Interface to all Light Sources having common methods
         */
        class ILight {
        public:
            virtual ~ILight();
            
            /**
             * Calculates the total Power (watts) of this Luminaire
             * @return Power in Watts
             */
            virtual float power() const = 0;
            
            /**
             * Radiance (L) is the flux that leaves a surface, 
             * per unit projected area of the surface, per unit solid angle of direction
             * Ω = (S/r²)
             * 
             * Radiance is Watts per Surface Area (of source) per Steradian (of source)
             * Returns the radiance (Watts per Meters Squared per Steradian) along 
             * the ray. We will assume a differential surface area which is infinitesimally small
             * at the source and at the detector, hence dividing by Area can be omitted,
             * since the same Infinitesimal Area would be multiplied afterwards. This can
             * sometimes be seen as Radiant Intensity but it is not..
             * @param ray The unbounded incident ray  whose inverse (-ray) would become the ray going 
             * out of this luminaire.
             * @return The Spectrum containing the Radiance
             */
            virtual void radiance(const Mathematics::Ray& ray, Spectrum::RgbSpectrum& a_spectrum) const = 0;

            virtual const Mathematics::Vector& getCentrePoint() const = 0;
        };
    }
}



#endif	/* ILIGHT_H */

