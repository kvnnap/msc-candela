/* 
 * File:   PointLight.cpp
 * Author: Kevin
 * 
 * Created on August 25, 2014, 8:53 PM
 */

#include "Light/PointLight.h"
#include "Mathematics/Constants.h"

using namespace std;
using namespace Candela::Light;
using namespace Candela::Spectrum;
using namespace Candela::Mathematics;

PointLight::PointLight(const Mathematics::Vector& point, unique_ptr<Spectrum::ISpectrum> a_spectrum)
    : point ( point ), spectrum ( move(a_spectrum) )
{
    //divide the power by a sphere total steradiance.
    //(*spectrum) *= Candela::Mathematics::Constants::OneOverFourPi;
}

float PointLight::power() const {
    //calculate power in watts using spectrum
    return 0.f;
}

void PointLight::radiance(const Mathematics::Ray& ray, Spectrum::RgbSpectrum& a_spectrum) const {
    //wherever the ray is coming from, we have same radiance,
    //no cosine required because a point Light is an infinitesimally small Sphere
    //Mathematics::Vector v = point - ray.getPosition();
    //spectrum->divide(v * v, a_spectrum);
    a_spectrum = spectrum->toTristimulusSRGB();
}

const Vector &PointLight::getCentrePoint() const {
    return point;
}


