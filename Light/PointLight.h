/* 
 * File:   PointLight.h
 * Author: Kevin
 *
 * Created on August 25, 2014, 8:53 PM
 */

#ifndef POINTLIGHT_H
#define	POINTLIGHT_H

#include "ILight.h"
#include "Spectrum/Spectrum.h"

namespace Candela
{
    namespace Light
    {
        class PointLight 
            : public ILight{
        public:
            PointLight(const Mathematics::Vector& point, std::unique_ptr<Spectrum::ISpectrum> spectrum);

            float power() const override;
            void radiance(const Mathematics::Ray& ray, Spectrum::RgbSpectrum& a_spectrum) const override;
            const Mathematics::Vector& getCentrePoint() const override;

        private:
            const Mathematics::Vector point;
            std::unique_ptr<Spectrum::ISpectrum> spectrum;
        };
    }
}



#endif	/* POINTLIGHT_H */

