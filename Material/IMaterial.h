//
// Created by kvnna on 25/06/2016.
//

#ifndef MSC_CANDELA_IMATERIAL_H
#define MSC_CANDELA_IMATERIAL_H

#include <string>
#include "Texture/ITexture.h"

namespace Candela
{
    namespace Material
    {
        class IMaterial {
        public:
            virtual ~IMaterial();

            virtual std::string toString() const = 0;
            virtual std::string getMaterialName() const = 0;

            virtual uint8_t getIlluminationMode() const = 0;
            virtual float getSpecularExponent() const = 0;
            virtual float getRefractiveIndex() const = 0;

            virtual const Texture::ITexture& getAmbientTexture() const = 0;
            virtual const Texture::ITexture& getDiffuseTexture() const = 0;
            virtual const Texture::ITexture& getSpecularTexture() const = 0;

        };
    }
}



#endif //MSC_CANDELA_IMATERIAL_H
