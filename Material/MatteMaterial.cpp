//
// Created by kvnna on 26/06/2016.
//

#include "MatteMaterial.h"

using namespace Candela::Material;
using namespace Candela::Texture;


MatteMaterial::MatteMaterial(std::unique_ptr<Candela::Spectrum::ISpectrum> spectrum)
    : matteTexture (move(spectrum))
{ }

std::string MatteMaterial::toString() const {
    return "Matte Material";
}

std::string MatteMaterial::getMaterialName() const {
    return "Unnamed Matte Material";
}

uint8_t MatteMaterial::getIlluminationMode() const {
    return 0;
}

float MatteMaterial::getSpecularExponent() const {
    return 0.f;
}

float MatteMaterial::getRefractiveIndex() const {
    return 1.f;
}

const ITexture &MatteMaterial::getAmbientTexture() const {
    return MatteTexture::black;
}

const ITexture &MatteMaterial::getDiffuseTexture() const {
    return matteTexture;
}

const ITexture &MatteMaterial::getSpecularTexture() const {
    return MatteTexture::black;
}

















