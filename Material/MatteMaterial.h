//
// Created by kvnna on 26/06/2016.
//

#ifndef MSC_CANDELA_MATTEMATERIAL_H
#define MSC_CANDELA_MATTEMATERIAL_H

#include <memory>
#include <Texture/MatteTexture.h>
#include "IMaterial.h"

namespace Candela
{
    namespace Material
    {
        class MatteMaterial
            : public IMaterial
        {
        public:

            MatteMaterial(std::unique_ptr<Spectrum::ISpectrum> spectrum);

            std::string toString() const override;
            std::string getMaterialName() const override;
            uint8_t getIlluminationMode() const override;
            float getSpecularExponent() const override;
            float getRefractiveIndex() const override;

            const Texture::ITexture& getAmbientTexture() const override;
            const Texture::ITexture& getDiffuseTexture() const override;
            const Texture::ITexture& getSpecularTexture() const override;

        private:
            Texture::MatteTexture matteTexture;
        };
    }
}


#endif //MSC_CANDELA_MATTEMATERIAL_H
