/* 
 * File:   Constants.cpp
 * Author: Kevin
 * 
 * Created on 28 December 2013, 14:07
 */

#include "Mathematics/Constants.h"

using namespace Candela::Mathematics;

const float Constants::Pi = 3.14159265358979323846;
const float Constants::PiOver2 = 1.57079632679489661923;
const float Constants::PiOver4 = 0.785398163397448309616;
const float Constants::OneOverPi = 0.318309886183790671538;
const float Constants::TwoOverPi = 0.636619772367581343076;
const float Constants::Ln2 = 0.693147180559945309417;
const float Constants::Ln10 = 2.30258509299404568402;
const float Constants::E = 2.71828182845904523536;
/* Calculator Computed */
const float Constants::OneOverLn2 = 1.4426950408889634073599246810019;
const float Constants::OneOverLn10 = 0.43429448190325182765112891891661;
const float Constants::FourPi = 12.566370614359172953850573533118;
const float Constants::OneOverFourPi = 0.07957747154594766788444188168626;

