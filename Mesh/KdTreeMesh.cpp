//
// Created by kvnna on 02/07/2016.
//

#include <algorithm>
#include <iostream>
#include <Mathematics/Float.h>
#include "KdTreeMesh.h"

using namespace std;
using namespace Candela::Mesh;
using namespace Candela::Shape;
using namespace Candela::Mathematics;

KDTreeMeshNode::KDTreeMeshNode()
    : Partition (), Axis ()
{ }

ShapeAxisSorter::ShapeAxisSorter(uint8_t axis)
        : axis ( axis )
{ }

bool ShapeAxisSorter::operator()(const Shape::IShape * a, const Shape::IShape * b) {
    AxisAlignedBoundingBox firstAabb, secondAabb;
    a->getBounds(firstAabb);
    b->getBounds(secondAabb);
    return firstAabb.getMinPosition().xyz[axis] < secondAabb.getMinPosition().xyz[axis];
}

// KD TREE

KdTreeMesh::KdTreeMesh(size_t p_maxDepth, size_t p_maxShapesInLeaf)
        : maxDepth (p_maxDepth), maxShapesInLeaf (p_maxShapesInLeaf)
{ }

void KdTreeMesh::getBounds(Shape::AxisAlignedBoundingBox &aabb) const {
    aabb = bounds;
}

void KdTreeMesh::add(const std::vector<const Shape::IShape *> &shapes) {
    // We have shapes, Construct bounding box
    root = unique_ptr<KDTreeMeshNode>(new KDTreeMeshNode());
    bounds.contain(shapes);
    bounds.spaceOut(0.01f);
    cout << "KD Bounds" << bounds << endl << " sizeof: " << sizeof(KDTreeMeshNode) << endl;
    build(root.get(), shapes, bounds, 0);
    cout << "done building" << endl;
}

void KdTreeMesh::build(KDTreeMeshNode *node, const vector<const IShape *> &shapes,
                       const AxisAlignedBoundingBox &aabb, size_t depth)
{
    if(depth != maxDepth && shapes.size() > maxShapesInLeaf)
    {
        uint8_t axis = static_cast<uint8_t>(depth % 3);

        // split plane using some algorithm
        node->Partition = split(shapes, aabb, axis, Mean);

        // split the AABB
        AxisAlignedBoundingBox left, right;
        aabb.splitUsingPartitionInAxis(axis, node->Partition, left, right);

        // Partition the shapes
        vector<const IShape *> leftShapes, rightShapes;
        for (const IShape* shape : shapes) {
            if (shape->partOf(left)) {
                leftShapes.push_back(shape);
            }
            if (shape->partOf(right)) {
                rightShapes.push_back(shape);
            }
        }

//        cout << "Total:" << shapes.size() << ", left: " << leftShapes.size() << " right: " << rightShapes.size() <<
//                " Partition: " << node->Partition << " axis: " << to_string(axis) << endl;

        // Recurse
        node->LeftChild = unique_ptr<KDTreeMeshNode>(new KDTreeMeshNode());
        node->RightChild = unique_ptr<KDTreeMeshNode>(new KDTreeMeshNode());
        build(node->LeftChild.get(), leftShapes, left, depth + 1);
        build(node->RightChild.get(), rightShapes, right, depth + 1);

    } else {
        // base case
        node->ShapeList = unique_ptr<std::vector<const IShape*>>(new vector<const IShape*>());
        (*node->ShapeList) = shapes;
    }
}

float KdTreeMesh::split(const std::vector<const Shape::IShape *> &shapes, const Shape::AxisAlignedBoundingBox &aabb,
                        uint8_t axis, SplittingStrategy strategy) {
    switch (strategy) {
        case Mean:
            return splitMean(aabb, axis);
        case Median:
            return splitMedian(shapes, aabb, axis);
        default:
            throw runtime_error("Unsupported Split Request");
    }
}

float KdTreeMesh::splitMean(const Shape::AxisAlignedBoundingBox &aabb,
                            uint8_t axis) const {
    return (aabb.getMaxPosition().xyz[axis] + aabb.getMinPosition().xyz[axis]) * 0.5f;
}

float KdTreeMesh::splitMedian(const std::vector<const Shape::IShape *> &shapes,
                              const Shape::AxisAlignedBoundingBox &aabb, uint8_t axis) const {
    // Sort on axis
    vector<const Shape::IShape*> myShapes = shapes;
    sort(myShapes.begin(), myShapes.end(), ShapeAxisSorter(axis));

    // Find partitioning plane
    if (myShapes.size() % 2 == 0) {
        // Even
        AxisAlignedBoundingBox left, right;
        myShapes.at(myShapes.size() / 2 - 1)->getBounds(left);
        myShapes.at(myShapes.size() / 2)->getBounds(right);
        return ( left.getMaxPosition().xyz[axis] + right.getMinPosition().xyz[axis] ) * 0.5f;
    } else {
        // Odd
        AxisAlignedBoundingBox middle;
        myShapes.at(myShapes.size() / 2)->getBounds(middle);
        return ( middle.getMaxPosition().xyz[axis] - middle.getMinPosition().xyz[axis] ) * 0.5f;
    }
}



bool KdTreeMesh::intersect(const BoundedRay &ray, Intersection &intersection) const {
    return intersectSimple(ray, intersection);
//    BoundedRay rayBox = ray;
//    float a ( Float::Infinity ), b (Float::Infinity);
//
//    // Find ray entry and exit point
//    if (bounds.contains(ray.getMinPoint())) {
//        // Inside the bounding box
//        Candela::Shape::Intersection localIntersection;
//        a = ray.getMin();
//        b = bounds.intersect(rayBox, localIntersection) ?
//            localIntersection.Distance : rayBox.getMax();
//    } else {
//        // Outside the bounding box
//        Candela::Shape::Intersection localIntersection;
//        if (bounds.intersect(rayBox, localIntersection) ) {
//            a = localIntersection.Distance;
//            rayBox.setMinAndAdvanceByEpsilon(a);
//            b = bounds.intersect(rayBox, localIntersection) ?
//                localIntersection.Distance : rayBox.getMax();
//        } else {
//            return false;
//        }
//    }
//
//    // Start
//    float tTemp ( b );
//
//    StackElem st[30];
//    uint8_t sp ( 0 );
//    KDTreeMeshNode *farChild = nullptr, *nearChild = nullptr, *currNode = nullptr;
//
//    // Init stack
//    st[0].node = root.get();
//    st[0].a = a;
//    st[0].b = b; ++sp;
//
//    while(sp)
//    {
//        {
//            const StackElem& p = st[--sp];
//            currNode = p.node; a = p.a; b = p.b;
//        }
//        while(currNode->LeftChild)//NOT LEAF
//        {
//            // Todo Check ray here
//            float diff ( currNode->Partition - rayBox.getPosition().xyz[currNode->Axis] );
//            if(diff == 0.f)
//            {
//                currNode = rayBox.getDirection().xyz[currNode->Axis] > 0 ?
//                           currNode->RightChild.get() : currNode->LeftChild.get();
//                continue;
//            }
//            else if(diff > 0)
//            {
//                nearChild = currNode->LeftChild.get();
//                farChild = currNode->RightChild.get();
//            }else
//            {
//                nearChild = currNode->RightChild.get();
//                farChild = currNode->LeftChild.get();
//            }
//
//            if(rayBox.getDirection().xyz[currNode->Axis] != 0)
//            {
//                float tSplit ( diff / rayBox.getDirection().xyz[currNode->Axis] ); //ATTENTION
//
//                if( (tSplit > b) || (tSplit < 0) )
//                {
//                    currNode = nearChild;
//                }else //tSplit >= 0 && tSplit <= b
//                {
//                    if(tSplit < a)//tSplit >= 0 && tSplit <= b && tSplit < a
//                    {
//                        currNode = farChild;
//                    }
//                    else //case 4 - //tSplit >= 0 && tSplit >= a && tSplit <= b
//                    {
//                        st[sp].node = farChild;
//                        st[sp].a = tSplit;
//                        st[sp++].b = b;
//                        if(sp == 30){ throw 30; }
//                        currNode = nearChild;
//                        b = tSplit;
//                    }
//                }
//            } else {
//                currNode = nearChild;
//            }
//        }
//
//        // Leaf - Check Prim List
//        if (currNode->ShapeList->size() > 0) {
//            rayBox.setMin(a);
//            rayBox.setMax(tTemp);
//            for (const IShape* shape : *currNode->ShapeList) {
//                if (shape->intersect(rayBox, intersection)) {
//                    rayBox.setMax(tTemp = intersection.Distance);
//                }
//            }
//        }
//
//        // Keep this order for performance
//        if(tTemp <= b && tTemp >= a)
//        {
//            return intersection.Type != Shape::Intersection::None;
//        }
//    }
//
//    // Stack empty, found nothing
//    return false;
}

float KdTreeMesh::getSurfaceArea() const {
    return bounds.getSurfaceArea();
}

float KdTreeMesh::getVolume() const {
    return bounds.getVolume();
}

// Todo: Check and implement or remove
void KdTreeMesh::contain(const IShape &shape) {
    throw runtime_error("Unsupported for Kd Tree");
}

void KdTreeMesh::contain(const vector<const IShape *> &shapes) {
    throw runtime_error("Unsupported for Kd Tree");
}

void KdTreeMesh::add(const Shape::IShape &shape) {
    throw runtime_error("Unsupported for Kd Tree");
}

bool KdTreeMesh::remove(const Shape::IShape &shape) {
    return false;
}

void KdTreeMesh::clear() {
    bounds = AxisAlignedBoundingBox();
    root.reset();
}

Vector KdTreeMesh::getUnitNormal(const Shape::Intersection &intersection) const {
    return Vector();
}

bool KdTreeMesh::intersectSimple(const Mathematics::BoundedRay &ray, Shape::Intersection &intersection) const {

    BoundedRay rayBox = ray;

    float a, b;
    // Find bounds
    if (bounds.contains(ray.getMinPoint())) {
        // Inside the bounding box
        Candela::Shape::Intersection localIntersection;
        a = ray.getMin();
        b = bounds.intersect(rayBox, localIntersection) ?
            localIntersection.Distance : rayBox.getMax();
    } else {
        // Outside the bounding box
        Candela::Shape::Intersection localIntersection;
        if (bounds.intersect(rayBox, localIntersection) ) {
            a = localIntersection.Distance;
            rayBox.setMinAndAdvanceByEpsilon(a);
            b = bounds.intersect(rayBox, localIntersection) ?
                localIntersection.Distance : rayBox.getMax();
        } else {
            return false;
        }
    }

    //size_t count = 0;

    while (rayBox.getMin() < b) {

        rayBox.setMax(b);

        AxisAlignedBoundingBox aabb = bounds;
        KDTreeMeshNode *node = getNodeFromPoint(root.get(), aabb, rayBox.getMinPoint());

//        if (++count == 200) {
//            cout << "Stuck" << endl;
//        }

        if (node) {
            // Intersect the box
            Candela::Shape::Intersection boxIntersection;

            // Get next maxDistance if available
            if (aabb.intersect(rayBox, boxIntersection)) {
                rayBox.setMax(boxIntersection.Distance);
            }

            bool shapeIntersected = false;
            // Intersect the shapes
            if (node->ShapeList->size() > 0) {
                for (const IShape *shape : *node->ShapeList) {
                    if (shape->intersect(rayBox, intersection)) {
                        rayBox.setMax(intersection.Distance);
                        shapeIntersected = true;
                    }
                }
            }
            if (shapeIntersected) {
                return true;
            }

        } else {
            return false;
        }

        rayBox.setMinAndAdvanceByEpsilon(rayBox.getMax());
    }

    return false;
}

KDTreeMeshNode *KdTreeMesh::getNodeFromPoint(KDTreeMeshNode *node,
                                             Shape::AxisAlignedBoundingBox &aabb,
                                             const Vector& point
) {
    // Given point, find Node
    uint8_t axis = 0;
    if (!aabb.contains(point)) return nullptr;
    while (node && node->LeftChild) {
        if (point.xyz[axis] < node->Partition) {
            aabb.setMaxAxis(axis, node->Partition);
            node = node->LeftChild.get();
        } else {
            aabb.setMinAxis(axis, node->Partition);
            node = node->RightChild.get();
        }
        axis = static_cast<uint8_t>((axis + 1) % 3);
    }
    return node;
}






