//
// Created by kvnna on 02/07/2016.
//

#ifndef MSC_CANDELA_KDTREEMESH_H
#define MSC_CANDELA_KDTREEMESH_H

#include <memory>
#include "IMesh.h"
#include "Shape/AxisAlignedBoundingBox.h"

namespace Candela
{
    namespace Mesh
    {

        struct KDTreeMeshNode
        {
            KDTreeMeshNode();
            std::unique_ptr<std::vector<const Shape::IShape*>> ShapeList;
            std::unique_ptr<KDTreeMeshNode> LeftChild;
            std::unique_ptr<KDTreeMeshNode> RightChild;
            float Partition;
            uint8_t Axis;
        };

        struct ShapeAxisSorter
        {
            ShapeAxisSorter(uint8_t axis);
            bool operator() (const Shape::IShape * a, const Shape::IShape * b);
        private:
            uint8_t axis;
        };

        struct StackElem {
            KDTreeMeshNode * node;
            float a, b;
        };

        class KdTreeMesh
            : public Mesh::IMesh
        {
        public:

            enum SplittingStrategy {
                Mean,
                Median
            };

            KdTreeMesh(size_t p_maxDepth, size_t p_maxShapesInLeaf);

            /* IShape */
            bool intersect(const Mathematics::BoundedRay& ray, Shape::Intersection& intersection) const override;
            float getSurfaceArea() const override;
            void getBounds(Shape::AxisAlignedBoundingBox& aabb) const override;
            Mathematics::Vector getUnitNormal(const Shape::Intersection& intersection) const override;

            /* IBoundingShape */
            float getVolume() const override;
            void contain(const Shape::IShape& shape) override;
            void contain(const std::vector<const Shape::IShape*>& shapes) override;


            /* IMesh */
            void add(const std::vector<const Shape::IShape*>& shapes) override;
            void add(const Shape::IShape& shape) override;
            bool remove(const Shape::IShape& shape) override;
            void clear() override;

            /* KdTreeMesh */
            bool intersectSimple(const Mathematics::BoundedRay& ray, Shape::Intersection& intersection) const;
            static KDTreeMeshNode * getNodeFromPoint(KDTreeMeshNode * node,
                                                     Shape::AxisAlignedBoundingBox & aabb,
                                                     const Mathematics::Vector& point
            );

        private:

            void build(KDTreeMeshNode* node,
                       const std::vector<const Shape::IShape*>& shapes,
                       const Shape::AxisAlignedBoundingBox& aabb,
                       size_t depth);

            // Splitting functions
            float split(const std::vector<const Shape::IShape*>& shapes,
                        const Shape::AxisAlignedBoundingBox& aabb,
                        uint8_t axis, SplittingStrategy strategy);
            float splitMean(const Shape::AxisAlignedBoundingBox& aabb,
                              uint8_t axis) const;
            float splitMedian(const std::vector<const Shape::IShape*>& shapes,
                              const Shape::AxisAlignedBoundingBox& aabb,
                              uint8_t axis) const;

            std::unique_ptr<KDTreeMeshNode> root;
            Shape::AxisAlignedBoundingBox bounds;
            size_t maxDepth;
            size_t maxShapesInLeaf;
        };

    }
}




#endif //MSC_CANDELA_KDTREEMESH_H
