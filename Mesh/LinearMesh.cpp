/* 
 * File:   LinearMesh.cpp
 * Author: Kevin
 * 
 * Created on 14 September 2014, 16:06
 */

#include <stdexcept>

#include "Mesh/LinearMesh.h"

using namespace std;
using namespace Candela::Shape;
using namespace Candela::Mesh;
using namespace Candela::Mathematics;

float LinearMesh::getSurfaceArea() const {
    return 0.f;
}

bool LinearMesh::intersect(const Mathematics::BoundedRay& ray, Shape::Intersection& intersection) const {
    Mathematics::BoundedRay tempRay ( ray );

    for (const IShape* shape : meshList_) {
        if(shape->intersect(tempRay, intersection)){
            tempRay.setMax(intersection.Distance);
        }
    }
    
    //check if any intersections
    if(intersection.Type != Shape::Intersection::None)
    {
        return true;
    }else{
        return false;
    }
}

void LinearMesh::add(const Shape::IShape& shape) {
    meshList_.push_back(&shape);
}

bool LinearMesh::remove(const Shape::IShape& shape) {
    if(meshList_.size() != 0)
    {
        for(unsigned i = 0; i < meshList_.size(); ++i)
        {
            if(meshList_[i] == &shape)
            {
                meshList_.erase(meshList_.begin() + i);
                return true;
            }
        }
        return false;
    }
    else
    {
        return false;
    }
}

void LinearMesh::clear() {
    meshList_.clear();
}

float LinearMesh::getVolume() const {
    return 0.f;
}

void LinearMesh::contain(const Shape::IShape &shape) {
    throw runtime_error("Linear Mesh, not implemented");
}

void LinearMesh::contain(const std::vector<const Shape::IShape *> &shapes) {
    throw runtime_error("Linear Mesh, not implemented");
}

void LinearMesh::add(const std::vector<const Shape::IShape *> &shapes) {
    meshList_.insert(meshList_.end(), shapes.begin(), shapes.end());
}

Vector LinearMesh::getUnitNormal(const Shape::Intersection &intersection) const {
    return Mathematics::Vector3<float>();
}

