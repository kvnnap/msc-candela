/* 
 * File:   LinearMesh.h
 * Author: Kevin
 *
 * Created on 14 September 2014, 16:06
 */

#ifndef LINEARMESH_H
#define	LINEARMESH_H

#include "IMesh.h"
#include <vector>
#include "../Shape/AxisAlignedBoundingBox.h"
#include "../Shape/IShape.h"

namespace Candela
{
    namespace Mesh
    {
        class LinearMesh 
            : public IMesh
        {
        public:
            /* IShape */
            virtual bool intersect(const Mathematics::BoundedRay& ray, Shape::Intersection& intersection) const override;
            virtual float getSurfaceArea() const override;
            Mathematics::Vector getUnitNormal(const Shape::Intersection& intersection) const;

            /* IBoundingShape */
            float getVolume() const override;
            void contain(const Shape::IShape& shape) override;
            void contain(const std::vector<const Shape::IShape*>& shapes) override;

            /* IMesh */
            void add(const std::vector<const Shape::IShape*>& shapes) override;
            void add(const Shape::IShape& shape) override;
            bool remove(const Shape::IShape& shape) override;
            void clear() override;
        private:
            //Shape::AxisAlignedBoundingBox boundingBox_;
            std::vector<const Shape::IShape *> meshList_;
        };
    }
}

#endif	/* LINEARMESH_H */

