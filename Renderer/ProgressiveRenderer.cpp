//
// Created by kvnna on 28/06/2016.
//

#include <iostream>
#include "ProgressiveRenderer.h"
#include "Sampler/UniformSampler.h"

using namespace std;
using namespace Candela::Renderer;
using namespace Candela::Mathematics;
using namespace Candela::Spectrum;
using namespace Candela::Sampler;

ProgressiveRenderer::ProgressiveRenderer()
        : iterations (1)
{ }

void ProgressiveRenderer::render() {

    BoundedRay primaryRay;
    UniformSampler sampler;

    // temp buffer
    unique_ptr<RadianceBuffer> tempRad (new RadianceBuffer(radianceBuffer->getWidth(), radianceBuffer->getHeight()));

    for (size_t n = 0; n < iterations; ++n) {
        for (size_t y = 0; y < radianceBuffer->getHeight(); ++y) {
            for (size_t x = 0; x < radianceBuffer->getWidth(); ++x) {
                // compute ray using Camera
                scene->getCamera()->getRayPixel(x, y,
                                                sampler.nextSample(), sampler.nextSample(),
                                                sampler.nextSample(), sampler.nextSample(),
                                                primaryRay
                );

                // Get Radiance Content
                RadianceContent &radianceContent = (*radianceBuffer)(x, y);

                // Save previous value so that we can accumulate
                RgbSpectrum rgbDirect (radianceContent.direct);
                RgbSpectrum rgbIndirect (radianceContent.indirect);
                RgbSpectrum rgbFinal (radianceContent.final);

                // Feed into integrator
                integrator->radiance(radianceContent, *scene, primaryRay);

                radianceContent.direct += rgbDirect;
                radianceContent.indirect += rgbIndirect;
                radianceContent.final += rgbFinal;
                //cout << x << ", "  << y << endl;
            }
            //cout << y << endl;
        }
        for (size_t y = 0; y < radianceBuffer->getHeight(); ++y) {
            for (size_t x = 0; x < radianceBuffer->getWidth(); ++x) {
                 (*radianceBuffer)(x, y).direct.divide(n + 1, (*tempRad)(x, y).direct);
                (*radianceBuffer)(x, y).indirect.divide(n + 1, (*tempRad)(x, y).indirect);
                (*radianceBuffer)(x, y).final.divide(n + 1, (*tempRad)(x, y).final);
            }
        }
        device->write(*tempRad, RadianceContent::FINAL);
        cout << n << endl;
    }

//    device->write(*radianceBuffer, RadianceContent::DIRECT);
//    device->write(*radianceBuffer, RadianceContent::INDIRECT);
}

void ProgressiveRenderer::setIterations(size_t p_iterations) {
    iterations = p_iterations;
}

size_t ProgressiveRenderer::getIterations() const {
    return 0;
}




