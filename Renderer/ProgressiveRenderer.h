//
// Created by kvnna on 28/06/2016.
//

#ifndef MSC_CANDELA_PROGRESSIVERENDERER_H
#define MSC_CANDELA_PROGRESSIVERENDERER_H


#include "Renderer.h"

namespace Candela
{
    namespace Renderer
    {
        class ProgressiveRenderer
                : public Renderer
        {
        public:

            ProgressiveRenderer();

            void setIterations(size_t p_iterations);
            size_t getIterations() const;

            // OVerrides
            void render() override;
        private:
            size_t iterations;
        };
    }
}

#endif //MSC_CANDELA_PROGRESSIVERENDERER_H
