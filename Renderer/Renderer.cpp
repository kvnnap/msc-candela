//
// Created by kvnna on 23/06/2016.
//

#include "Renderer.h"
#include "Mathematics/Rectangle.h"

using namespace Candela::Renderer;

Renderer::~Renderer() {

}

void Renderer::setScene(Scene::Scene *p_scene) {
    scene = p_scene;
    // Set appropriate Radiance Buffer dimensions
    const Mathematics::Rectangle_I& rect = p_scene->getCamera()->getPixelRect();
    radianceBuffer.reset(new RadianceBuffer(rect.getWidth(), rect.getHeight()));
}

void Renderer::setDevice(Device::IDevice *p_device) {
    device = p_device;
}

Candela::Scene::Scene *Renderer::getScene() {
    return scene;
}

Candela::Device::IDevice *Renderer::getDevice() {
    return device;
}

void Renderer::setIntegrator(Integrator::IIntegrator *p_integrator) {
    integrator = p_integrator;
}

Candela::Integrator::IIntegrator *Renderer::getIntegrator() {
    return integrator;
}




