//
// Created by kvnna on 23/06/2016.
//

#ifndef MSC_CANDELA_RENDERER_H
#define MSC_CANDELA_RENDERER_H

#include <Scene/Scene.h>
#include <Device/IDevice.h>
#include <Integrator/IIntegrator.h>

namespace Candela
{
    namespace Renderer
    {

        /**
         * A renderer renders a scene. Therefore:
         * 1) Requires a scene object.
         * 2) Requires an integrator, that gives back radiance for each pixel
         * 3) Requires a buffer/device to output this data on
         */
        class Renderer {
        public:
            virtual ~Renderer();

            void setScene(Scene::Scene * p_scene);
            Scene::Scene * getScene();

            void setDevice(Device::IDevice * p_device);
            Device::IDevice * getDevice();

            void setIntegrator(Integrator::IIntegrator * p_integrator);
            Integrator::IIntegrator * getIntegrator();

            virtual void render() = 0;

        protected:
            // Concrete scene
            Scene::Scene * scene;

            // Device to output the result to
            Device::IDevice * device;

            // Integrator
            Integrator::IIntegrator * integrator;

            // Buffer
            std::unique_ptr<RadianceBuffer> radianceBuffer;
        };
    }
}




#endif //MSC_CANDELA_RENDERER_H
