//
// Created by kvnna on 23/06/2016.
//

#include "SimpleRenderer.h"

using namespace Candela::Renderer;
using namespace Candela::Mathematics;

void SimpleRenderer::render() {
    BoundedRay primaryRay;

    for (size_t y = 0; y < radianceBuffer->getHeight(); ++y)
    {
        for (size_t x = 0; x < radianceBuffer->getWidth(); ++x)
        {
            // compute ray using Camera
            scene->getCamera()->getRayPixel(x, y, primaryRay);

            // Get Radiance Content
            RadianceContent& radianceContent = (*radianceBuffer)(x, y);

            // Feed into integrator
            integrator->radiance(radianceContent, *scene, primaryRay);
        }
    }

    device->write(*radianceBuffer, RadianceContent::FINAL);
}
