//
// Created by kvnna on 23/06/2016.
//

#ifndef MSC_CANDELA_SIMPLERENDERER_H
#define MSC_CANDELA_SIMPLERENDERER_H

#include "Renderer.h"

namespace Candela
{
    namespace Renderer
    {
        class SimpleRenderer
            : public Renderer
        {
        public:
            void render() override;
        };
    }
}


#endif //MSC_CANDELA_SIMPLERENDERER_H
