//
// Created by kvnna on 22/06/2016.
//

#ifndef MSC_CANDELA_ISAMPLER_H
#define MSC_CANDELA_ISAMPLER_H

#include <stddef.h>
#include <vector>

namespace Candela
{
    namespace Sampler
    {
        class ISampler {
        public:
            virtual ~ISampler();

            virtual float nextSample() = 0;
            virtual float nextSample(float min, float max) = 0;
            virtual std::vector<float> nextSamples(size_t p_numSamples) = 0;
        };
    }
}



#endif //MSC_CANDELA_ISAMPLER_H
