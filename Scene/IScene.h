/* 
 * File:   IScene.h
 * Author: Kevin
 *
 * Created on 08 September 2014, 16:16
 */

#ifndef ISCENE_H
#define	ISCENE_H

#include "Mathematics/BoundedRay.h"
#include "../Shape/Intersection.h"

namespace Candela
{
    namespace Scene
    {
        class IScene {
        public:
            virtual ~IScene();
            
            //virtual bool intersect(const Mathematics::BoundedRay& ray, Shape::Intersection& intersection) const = 0;
        private:
            
        };
    }
}



#endif	/* ISCENE_H */

