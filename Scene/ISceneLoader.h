//
// Created by kvnna on 27/06/2016.
//

#ifndef MSC_CANDELA_ISCENELOADER_H
#define MSC_CANDELA_ISCENELOADER_H

#include "Scene/Scene.h"

namespace Candela {
    namespace Scene {
        class ISceneLoader {
        public:
            virtual ~ISceneLoader();

            // Load
            virtual void load(Scene& scene) = 0;
        };
    }
}

#endif //MSC_CANDELA_ISCENELOADER_H
