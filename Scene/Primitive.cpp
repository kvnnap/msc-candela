//
// Created by kvnna on 25/06/2016.
//

#include "Primitive.h"

using namespace std;
using namespace Candela::Shape;
using namespace Candela::Material;
using namespace Candela::Scene;
using namespace Candela::Mathematics;

Primitive::Primitive(const IShape *p_shape, const IMaterial *p_material)
        : shape (p_shape), material (p_material)
{
}

const IMaterial* Primitive::getMaterial() const {
    return material;
}

const IShape* Primitive::getShape() const {
    return shape;
}

bool Primitive::hasMaterial() const {
    return material == nullptr;
}

bool Primitive::hasShape() const {
    return shape == nullptr;
}

void Primitive::setMaterial(IMaterial * p_material){
    material = p_material;
}

void Primitive::setShape(IShape * p_shape) {
    shape = p_shape;
}

bool Primitive::intersect(const BoundedRay &ray, Scene::Intersection &intersection) const {
    if (shape->intersect(ray, intersection.shapeIntersection))
    {
        // Associate primitive
        intersection.primitive = this;
        return true;
    }
    return false;
}

bool Primitive::intersect(const BoundedRay &ray, Shape::Intersection &intersection) const {
    if (shape->intersect(ray, intersection)) {
        intersection.Intersectant = this;
        return true;
    }
    return false;
}

float Primitive::getSurfaceArea() const {
    return shape->getSurfaceArea();
}

Vector Primitive::getUnitNormal(const Shape::Intersection &intersection) const {
    return shape->getUnitNormal(intersection);
}

void Primitive::getBounds(Shape::AxisAlignedBoundingBox &aabb) const {
    return shape->getBounds(aabb);
}

bool Primitive::partOf(const Shape::AxisAlignedBoundingBox &aabb) const {
    return shape->partOf(aabb);
}




