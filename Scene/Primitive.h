//
// Created by kvnna on 25/06/2016.
//

#ifndef MSC_CANDELA_PRIMITIVE_H
#define MSC_CANDELA_PRIMITIVE_H

#include "Shape/IShape.h"
#include "Material/IMaterial.h"
#include "Intersection.h"

namespace Candela
{
    namespace Scene
    {
        class Primitive
            : public Shape::IShape
        {
        public:

            Primitive(const Shape::IShape * p_shape = nullptr, const Material::IMaterial * p_material = nullptr);

            const Material::IMaterial* getMaterial() const;
            const Shape::IShape* getShape() const;

            void setMaterial(Material::IMaterial * p_material);
            void setShape(Shape::IShape * p_shape);

            bool hasMaterial() const;
            bool hasShape() const;

            // Primitive Intersect
            bool intersect(const Mathematics::BoundedRay& ray, Intersection& intersection) const;

            // Shape overrides
            bool intersect(const Mathematics::BoundedRay& ray, Shape::Intersection& intersection) const override;
            Mathematics::Vector getUnitNormal(const Shape::Intersection& intersection) const override;
            float getSurfaceArea() const override;
            void getBounds(Shape::AxisAlignedBoundingBox& aabb) const override;
            bool partOf(const Shape::AxisAlignedBoundingBox& aabb) const override;

        private:
            // And it owns a shape as well
            const Shape::IShape * shape;

            // A primitive owns the material
            const Material::IMaterial * material;
        };
    }
}



#endif //MSC_CANDELA_PRIMITIVE_H
