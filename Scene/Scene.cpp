/* 
 * File:   Scene.cpp
 * Author: Kevin
 * 
 * Created on 08 September 2014, 17:22
 */

#include "Scene/Scene.h"

using namespace std;
using namespace Candela::Camera;
using namespace Candela::Scene;
using namespace Candela::Light;
using namespace Candela::Material;
using namespace Candela::Shape;
using namespace Candela::Mesh;

Scene::Scene() {
}

void Scene::setCamera(unique_ptr<IMappedCamera> p_camera) {
    camera = move(p_camera);
}

size_t Scene::addLight(unique_ptr<ILight> light) {
    lights.push_back(move(light));
    return lights.size() - 1;
}

size_t Scene::addMaterial(unique_ptr<IMaterial> material) {
    materials.push_back(move(material));
    return materials.size() -1;
}

size_t Scene::addShape(unique_ptr<Shape::IShape> shape) {
    shapes.push_back(move(shape));
    return shapes.size() -1;
}

size_t Scene::addPrimitive(unique_ptr<Primitive> primitive) {
    primitives.push_back(move(primitive));
    return primitives.size() - 1;
}

size_t Scene::addPrimitive(size_t shapeId, size_t materialId) {
    primitives.push_back(unique_ptr<Primitive>(new Primitive(
            getShape(shapeId),
            getMaterial(materialId)
    )));
    return 0;
}

void Scene::setMesh(std::unique_ptr<Mesh::IMesh> p_mesh) {
    mesh = move(p_mesh);
}

const IMappedCamera *Scene::getCamera() const {
    return camera.get();
}

const ILight *Scene::getLight(size_t id) const {
    return lights[id].get();
}

const IMaterial *Scene::getMaterial(size_t id) const {
    return materials[id].get();
}

const IShape *Scene::getShape(size_t id) const {
    return shapes[id].get();
}

const Primitive *Scene::getPrimitive(size_t id) const {
    return primitives[id].get();
}

const vector<unique_ptr<Primitive>> &Scene::getPrimitives() const {
    return primitives;
}

const vector<unique_ptr<ILight>> &Scene::getLights() const {
    return lights;
}

const IMesh *Scene::getMesh() const {
    return mesh.get();
}

void Scene::commitToMesh() {
    // Commit Primitives to mesh
    if (mesh) {
        std::vector<const IShape *> nakedPointerPrimitives;
        for (unique_ptr<Primitive> &primUnPt : primitives) {
            nakedPointerPrimitives.push_back(primUnPt.get());
        }
        mesh->clear();
        mesh->add(nakedPointerPrimitives);
    } else {
        throw runtime_error("Scene: No mesh to commit to");
    }
}


