/* 
 * File:   Scene.h
 * Author: Kevin
 *
 * Created on 08 September 2014, 17:22
 */

#ifndef SCENE_H
#define	SCENE_H

#include "IScene.h"
#include "Mesh/IMesh.h"
#include "Light/ILight.h"
#include "Primitive.h"
#include "Shape/IShape.h"
#include "Camera/IMappedCamera.h"

#include <vector>
#include <memory>

namespace Candela
{
    namespace Scene
    {
        class Scene 
         : public IScene{
        public:
            Scene();

            void setCamera(std::unique_ptr<Camera::IMappedCamera> camera);
            size_t addLight(std::unique_ptr<Light::ILight> light);
            size_t addMaterial(std::unique_ptr<Material::IMaterial> material);
            size_t addShape(std::unique_ptr<Shape::IShape> shape);
            size_t addPrimitive(std::unique_ptr<Primitive> primitive);
            size_t addPrimitive(size_t shapeId, size_t materialId);
            void setMesh(std::unique_ptr<Mesh::IMesh> mesh);

            const Camera::IMappedCamera * getCamera() const ;
            const Light::ILight * getLight(size_t id) const;
            const Material::IMaterial * getMaterial(size_t id) const;
            const Shape::IShape * getShape(size_t id) const;
            const Primitive * getPrimitive(size_t id) const;
            const Mesh::IMesh * getMesh() const;

            const std::vector<std::unique_ptr<Primitive>>& getPrimitives() const;
            const std::vector<std::unique_ptr<Light::ILight>>& getLights() const;

            void commitToMesh();
            
        private:

            // Set the scene camera
            std::unique_ptr<Camera::IMappedCamera> camera;

            //lights don't need to be accelerated with a structure - Check
            std::vector<std::unique_ptr<Light::ILight>> lights;

            // List of Materials
            std::vector<std::unique_ptr<Material::IMaterial>> materials;

            // List of Raw Shapes - these include texture coordinates
            std::vector<std::unique_ptr<Shape::IShape>> shapes;

            // All the primitives contained in the scene
            // Note that a primitive could contain a mesh as a shape
            std::vector<std::unique_ptr<Primitive>> primitives;

            // Need acceleration structure over primitives here!
            std::unique_ptr<Mesh::IMesh> mesh;

        };
    }
}

#endif	/* SCENE_H */

