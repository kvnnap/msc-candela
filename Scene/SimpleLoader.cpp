//
// Created by kvnna on 29/06/2016.
//

#include <memory>


#include "Shape/Plane.h"
#include "Shape/Sphere.h"
#include "SimpleLoader.h"
#include "Camera/PinholeCamera.h"
#include "Light/PointLight.h"
#include "Spectrum/RgbSpectrum.h"
#include "Material/MatteMaterial.h"

using namespace std;
using namespace Candela::Scene;
using namespace Candela::Camera;
using namespace Candela::Light;
using namespace Candela::Shape;
using namespace Candela::Spectrum;
using namespace Candela::Mathematics;
using namespace Candela::Material;

void SimpleLoader::load(Scene& scene) {

    // Materials here
    size_t greenMatId = scene.addMaterial(unique_ptr<IMaterial>(new MatteMaterial(unique_ptr<RgbSpectrum>(new RgbSpectrum(0.1f, 0.95f, 0.1f)))));
    size_t whiteMatId = scene.addMaterial(unique_ptr<IMaterial>(new MatteMaterial(unique_ptr<RgbSpectrum>(new RgbSpectrum(1.f)))));

    // Shapes here
    size_t planeShapeId = scene.addShape(unique_ptr<IShape>(new Plane(Vector(0, -1.5f, 0), Vector::UnitY)));
    for (int i = 3; i < 6; ++i) {
        scene.addShape(unique_ptr<IShape>(new Sphere(Vector(i*5 - 20, 0.0f, 3 + ((i+1)%2)*3), 1.5f)));
    }

    // Primitives here
//    scene.addPrimitive(unique_ptr<Primitive>(new Primitive(scene.getShape(planeShapeId), scene.getMaterial(greenMatId))));
//    for (size_t sphereShapeId = 1; sphereShapeId <= 3; ++sphereShapeId) {
//        scene.addPrimitive(unique_ptr<Primitive>(new Primitive(scene.getShape(sphereShapeId), scene.getMaterial(whiteMatId))));
//    }

    scene.addPrimitive(planeShapeId, greenMatId);
    for (size_t sphereShapeId = 1; sphereShapeId <= 3; ++sphereShapeId) {
        scene.addPrimitive(sphereShapeId, whiteMatId);
    }

    scene.commitToMesh();
}

