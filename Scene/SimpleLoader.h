//
// Created by kvnna on 29/06/2016.
//

#ifndef MSC_CANDELA_SIMPLELOADER_H
#define MSC_CANDELA_SIMPLELOADER_H

#include "ISceneLoader.h"

namespace Candela
{
    namespace Scene
    {
        class SimpleLoader
            : public ISceneLoader
        {
        public:
            void load(Scene& scene) override;
        };
    }
}



#endif //MSC_CANDELA_SIMPLELOADER_H
