//
// Created by kvnna on 27/06/2016.
//

#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>

#include "Material/WavefrontMaterial.h"
#include "Mathematics/Matrix.h"
#include "Mathematics/Constants.h"
#include "Mathematics/Float.h"
#include "Shape/Triangle.h"
#include "Material/MatteMaterial.h"
#include "Texture/MatteTexture.h"
#include "WavefrontLoader.h"
#include "Mathematics/Funtions.h"
#include "Spectrum/RgbSpectrum.h"

using namespace std;
using namespace Candela::Scene;
using namespace Candela::Shape;
using namespace Candela::Spectrum;
using namespace Candela::Material;
using namespace Candela::Mathematics;
using namespace Candela::Texture;

WavefrontLoader::WavefrontLoader()
    : currentMaterialId (), useMaterial ()
{}

void WavefrontLoader::setFileName(const std::string &p_fileName) {
    fileName = p_fileName;
}

const string& WavefrontLoader::getFileName() const {
    return fileName;
}

void WavefrontLoader::load(Scene& scene) {
    // Open file
    ifstream objFile (fileName);

    // File exists?
    if (!objFile.good()) {
        throw runtime_error("Invalid Wavefront Obj File Path: " + fileName);
    }

    // Set Buffer size to 8KB
    char buff[8192];
    objFile.rdbuf()->pubsetbuf(buff, sizeof(buff));

    // Start parsing
    string line, type, tempStr;
    Vector tempVec;
    Vector2<float> tempVec2;
    while(getline(objFile, line)) {
        // Clean line
        line.erase(line.find_last_not_of("\r ") + 1);
        // get stream
        istringstream objLineStream (line);

        // Get the type of line
        if (!(objLineStream >> type)) {
            continue;
        }

        // Decide what to do
        if (type == "mtllib") {
            objLineStream >> tempStr; // Mtl path
            // Load Material function call
            loadMtl(scene, tempStr);
        } else if (type == "usemtl") {
            objLineStream >> tempStr; // Mtl Name
            currentMaterialId = materialMap.at(tempStr);
            useMaterial = true;
        } else if (type == "o") {

        } else if (type == "v") {
            objLineStream >> tempVec.x >> tempVec.y >> tempVec.z;
            v.push_back(tempVec);

        } else if (type == "vn") {
            objLineStream >> tempVec.x >> tempVec.y >> tempVec.z;
            vn.push_back(tempVec);

        } else if (type == "vt") {
            objLineStream >> tempVec2.x;
            if (!objLineStream.eof()) {
                objLineStream >> tempVec2.y;
            }
            // Clamp and repeat
            for (size_t i = 0; i < 2; ++i) {
                const float val = Functions::mod(tempVec2.xy[i], 1.f + Float::MachineEpsilon);
                tempVec2.xy[i] = val < 0 ? (val + 1.f) : (val);
            }
            vt.push_back(tempVec2);
        } else if (type == "f") {

            // Determine face format by reading first chunk
            VertexType vertType = SPATIAL;
            objLineStream >> tempStr;
            size_t slashCount = count(tempStr.begin(), tempStr.end(), '/');
            if (slashCount > 0) {
                vertType = static_cast<VertexType>(vertType | (slashCount == 1 ? TEXTURE : NORMAL));
                if (slashCount == 2 && tempStr.find("//") == string::npos) {
                    // Check whether we have to add TEXTURE
                    vertType = static_cast<VertexType>(vertType | TEXTURE);
                }
            }

            // Reload with previous state
            objLineStream.clear();
            objLineStream.str(line.substr(2, line.size() - 2));

            // We now know complete format, parse accordingly
            int vIndex[3], vtIndex[3], vnIndex[3];
            for (size_t i = 0; !objLineStream.eof(); ++i) {
                // load indices and rebase
                size_t currI = i >= 2 ? 2 : i;
                if (vertType & SPATIAL) {
                    objLineStream >> vIndex[currI];
                    vIndex[currI] = vIndex[currI] < 0 ? v.size() + vIndex[currI] : vIndex[currI] - 1;
                }
                if (vertType & TEXTURE) {
                    objLineStream.ignore(1, '/');
                    objLineStream >> vtIndex[currI];
                    vtIndex[currI] = vtIndex[currI] < 0 ? vt.size() + vtIndex[currI] : vIndex[currI] - 1;
                }
                if (vertType & NORMAL) {
                    if (!(vertType & TEXTURE)) {
                        objLineStream.ignore(1, '/');
                    }
                    objLineStream.ignore(1, '/');
                    objLineStream >> vnIndex[currI];
                    vnIndex[currI] = vnIndex[currI] < 0 ? vn.size() + vnIndex[currI] : vIndex[currI] - 1;
                }

                if (i >= 2) {
                    // Create actual shapes - assuming Triangle Fan
                    addTriangle(scene, vertType,
                                v[vIndex[0]], v[vIndex[1]], v[vIndex[2]],
                                vt[vtIndex[0]], vt[vtIndex[1]], vt[vtIndex[2]],
                                vn[vnIndex[0]], vn[vnIndex[1]], vn[vnIndex[2]]
                    );
                    // move
                    vIndex[1] = vIndex[2];
                    vtIndex[1] = vtIndex[2];
                    vnIndex[1] = vnIndex[2];
                }
            }
        }
    }

    // Commit to mesh
    scene.commitToMesh();
}

void WavefrontLoader::loadMtl(Scene& scene, const string &p_filename) {
    // Open file
    ifstream mtlFile (p_filename);

    // File exists?
    if (!mtlFile.good()) {
        throw runtime_error("Invalid Wavefront Obj File Path: " + fileName);
    }

    // Parse Material
    string line, type, temp;
    RgbSpectrum rgbTemp;
    Vector& rgbRef = rgbTemp.getAbcReference();
    unique_ptr<Material::WavefrontMaterial> material;
    while(getline(mtlFile, line)) {
        // Clean line
        line.erase(line.find_last_not_of("\r ") + 1);
        // get stream
        istringstream mtlLineStream (line);

        // Get the type of line
        if (!(mtlLineStream >> type)) {
            continue;
        }

        if(type == "newmtl")
        {
            // extract name
            mtlLineStream >> temp;
            // Add previous material to scene and to map
            if (material) {
                materialMap[material->getMaterialName()] = scene.addMaterial(move(material));
            }
            material.reset(new WavefrontMaterial());
            material->setMaterialName(temp);
        }else if(type == "illum")
        {
            uint8_t illuminationModel;
            mtlLineStream >> illuminationModel;
            material->setIlluminationModel(illuminationModel);
        }else if(type == "Ns")
        {
            float specExp;
            mtlLineStream >> specExp;
            material->setSpecularExponent(specExp);
        }else if(type == "Ni")
        {
            float refractiveIndex;
            mtlLineStream >> refractiveIndex;
            material->setRefractiveIndex(refractiveIndex);
        }else if(type == "Tf")
        {
            mtlLineStream >> rgbRef.x >> rgbRef.y >> rgbRef.z;
            material->setRefraction(rgbRef);
        }
        else if(type == "map_Ka")
        {
            mtlLineStream >> temp;
            unique_ptr<MemoryTexture> texture(new MemoryTexture());
            texture->loadFromFile(temp);
            material->setAmbientTexture(move(texture));
        }else if(type == "Ka")
        {
            mtlLineStream >> rgbRef.x >> rgbRef.y >> rgbRef.z;
            material->setAmbientTexture(unique_ptr<MatteTexture>(new MatteTexture(unique_ptr<RgbSpectrum>(
                    new RgbSpectrum(rgbRef)
            ))));
        }else if(type == "map_Kd")
        {
            mtlLineStream >> temp;
            unique_ptr<MemoryTexture> texture(new MemoryTexture());
            texture->loadFromFile(temp);
            material->setDiffuseTexture(move(texture));
        }else if(type == "Kd")
        {
            mtlLineStream >> rgbRef.x >> rgbRef.y >> rgbRef.z;
            material->setDiffuseTexture(unique_ptr<MatteTexture>(new MatteTexture(unique_ptr<RgbSpectrum>(
                    new RgbSpectrum(rgbRef)
            ))));
        }else if(type == "map_Ks")
        {
            mtlLineStream >> temp;
            unique_ptr<MemoryTexture> texture(new MemoryTexture());
            texture->loadFromFile(temp);
            material->setSpecularTexture(move(texture));
        }else if(type == "Ks")
        {
            mtlLineStream >> rgbRef.x >> rgbRef.y >> rgbRef.z;
            material->setSpecularTexture(unique_ptr<MatteTexture>(new MatteTexture(unique_ptr<RgbSpectrum>(
                    new RgbSpectrum(rgbRef)
            ))));
        }
    }
    if (material) {
        materialMap[material->getMaterialName()] = scene.addMaterial(move(material));
    }
}

void WavefrontLoader::addTriangle(Scene &scene, VertexType vt,
                                  const Vector         &v1, const Vector         &v2, const Vector         &v3,
                                  const Vector2<float> &t1, const Vector2<float> &t2, const Vector2<float> &t3,
                                  const Vector         &n1, const Vector         &n2, const Vector         &n3
) {
    size_t triMatId = useMaterial ? currentMaterialId :
                      scene.addMaterial(unique_ptr<IMaterial>(new MatteMaterial(unique_ptr<RgbSpectrum>(new RgbSpectrum(1.f)))));
    // rotate
    // Convert to Left-Hand side coordinate system
    Matrix_F rightToLeft (3,3);
    rightToLeft = rightToLeft.getIdentity();
    rightToLeft(0,0) = -1;
    //rightToLeft(2,2) = -1;
    // to rotate about y, simply negate x and z in every vector
    Triangle * tri = new Triangle(rightToLeft * v1, rightToLeft *  v2, rightToLeft *  v3);
    if (vt & TEXTURE) {
        tri->setTextureCoordinates(t1, t2, t3);
    }
    if (vt & NORMAL) {
        tri->setNormalCoordinates(rightToLeft * n1, rightToLeft * n2, rightToLeft * n3);
    }
    size_t triId = scene.addShape(unique_ptr<IShape>(tri));
    scene.addPrimitive(triId, triMatId);
//    cout << "Material: " << scene.getMaterial(triMatId)->getMaterialName() << endl;
//    cout << "Shape: " << *tri << endl;
}
