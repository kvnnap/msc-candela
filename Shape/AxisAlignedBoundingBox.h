/* 
 * File:   AxisAlignedBoundingBox.h
 * Author: Kevin
 *
 * Created on 15 March 2014, 22:38
 */

#ifndef AXISALIGNEDBOUNDINGBOX_H
#define	AXISALIGNEDBOUNDINGBOX_H

#include "IBoundingShape.h"

namespace Candela
{
    namespace Shape
    {
        class AxisAlignedBoundingBox 
                : public IBoundingShape
        {
        public:
            AxisAlignedBoundingBox();
            AxisAlignedBoundingBox(const Mathematics::Vector& minPosition,
                                   const Mathematics::Vector& maxPosition);
            AxisAlignedBoundingBox(const Mathematics::Vector& centre,
                                   float radius);
            
            /* Inherited from IShape */
            std::string toString() const override;
            bool intersect(const Mathematics::BoundedRay& ray, Intersection& intersection) const override;
            Mathematics::Vector getUnitNormal(const Intersection& intersection) const override;
            float getSurfaceArea() const override;
            void getBounds(AxisAlignedBoundingBox& aabb) const override;
            bool partOf(const AxisAlignedBoundingBox& aabb) const override;
            
            /* Inherited from IBoundingShape */
            virtual float getVolume() const;
            virtual void contain(const IShape& shape);
            virtual void contain(const std::vector<const IShape*>& shapes);
            bool contains(const Mathematics::Vector& point) const override;

            // Other
            void contain (const Mathematics::Vector& point);
            const Mathematics::Vector& getMinPosition() const;
            const Mathematics::Vector& getMaxPosition() const;
            void splitUsingPartitionInAxis(uint8_t axis,
                                           float partition,
                                           AxisAlignedBoundingBox& left,
                                           AxisAlignedBoundingBox& right) const;
            void spaceOut(float amount);
            void setMaxAxis(uint8_t axis, float value);
            void setMinAxis(uint8_t axis, float value);
            //void setBounds();
            
        private:
            Mathematics::Vector minPosition_, maxPosition_;
        };
    }
}

#endif	/* AXISALIGNEDBOUNDINGBOX_H */

