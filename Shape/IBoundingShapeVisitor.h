/* 
 * File:   IBoundingShapeVisitor.h
 * Author: Kevin
 *
 * Created on 26 December 2013, 17:34
 */

#ifndef IBOUNDINGSHAPEVISITOR_H
#define	IBOUNDINGSHAPEVISITOR_H

namespace Candela
{
    namespace Shape
    {
        class IBoundingShapeVisitor
        {
            //define a visitor for each one
            virtual void visit() = 0;
        };
    }
}

#endif	/* IBOUNDINGSHAPEVISITOR_H */

