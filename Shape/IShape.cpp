/* 
 * File:   IShape.cpp
 * Author: Kevin
 * 
 * Created on 22 December 2013, 21:39
 */

#include <stdexcept>
#include <sstream>

#include "Shape/IShape.h"

using namespace std;
using namespace Candela::Shape;

IShape::~IShape() {}

std::string IShape::toString() const
{
    using namespace std;
    ostringstream sstr;
    sstr << "IShape - Address = " << this;
    return sstr.str();
}

Candela::Mathematics::Vector2<float> IShape::getTextureUv(const Intersection &intersection) const {
    return Mathematics::Vector2<float>();
}

void IShape::getBounds(AxisAlignedBoundingBox &aabb) const {
    throw runtime_error("getBounds Not Implemented: ShapeInfo: " + toString());
}

bool IShape::partOf(const AxisAlignedBoundingBox &aabb) const {
    return false;
}

std::ostream& Candela::Shape::operator <<(std::ostream& strm, const IShape& ishape)
{
    return strm << ishape.toString();
}