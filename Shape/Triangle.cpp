/* 
 * File:   Triangle.cpp
 * Author: Kevin
 * 
 * Created on 02 March 2014, 18:00
 */

#include <sstream>
#include <cmath>

#include "Shape/Triangle.h"
#include "Shape/AxisAlignedBoundingBox.h"
#include "Mathematics/Vector2.h"
#include "Mathematics/Funtions.h"

using namespace Candela::Shape;
using namespace Candela::Mathematics;

Triangle::Triangle(const Mathematics::Vector& O, const Mathematics::Vector& A, const Mathematics::Vector& B) 
{
    vertex_[0] = O;
    vertex_[1] = A;
    vertex_[2] = B;
}

bool Triangle::intersect(const Mathematics::BoundedRay& ray, Intersection& intersection) const
{
    using namespace Candela::Mathematics;
    
    //these below are cache-able
    const Vector Q1 = vertex_[1] - vertex_[0];
    const Vector Q2 = vertex_[2] - vertex_[0];
    const Vector faceUnitNormal = ~(Q2 ^ Q1);
    
    //compute denominator
    const float denominator = faceUnitNormal * ray.getDirection();
    if(denominator != 0.f) // make this check or else risk of division by zero..
    {
        float temp = - (faceUnitNormal * (ray.getPosition() - vertex_[0])) / denominator;
        //make sure intersection is not behind the ray
        if(temp >= ray.getMin())
        {
            //this is closer than previous intersection
            if(temp <= ray.getMax())
            {
                //check whether it lies in the triangle
                const Vector R = ray.getPoint(temp) - vertex_[0];
                
                //below 4 values are cache-able
                const float Q1Sq = Q1 * Q1;
                const float Q2Sq = Q2 * Q2;
                const float Q1Q2 = Q1 * Q2;
                union{
                    float determinant;
                    float oneOverDeterminant;
                };
                
                determinant = Q1Sq * Q2Sq - Q1Q2 * Q1Q2;
                
                if(determinant != 0.f)
                {
                    oneOverDeterminant = 1.f / determinant;
                    const float RQ1 = R * Q1, RQ2 = R * Q2;
                    const float u = ( Q2Sq * RQ1 - Q1Q2 * RQ2 ) * oneOverDeterminant;
                    const float v = ( Q1Sq * RQ2 - Q1Q2 * RQ1 ) * oneOverDeterminant;
                    if( (u >= 0.f) && (v >= 0.f) && ((u + v) <= 1.f) )
                    //it is in triangle!
                    {
                        intersection.Intersectant = this;
                        intersection.Distance = temp;
                        intersection.Type = denominator < 0.f ?
                                    Intersection::External : Intersection::Internal;
                        // Since we already computed them, save them for later!
                        intersection.uv.x = u;
                        intersection.uv.y = v;
                        return true;
                    }
                }
            }
        }
    }
    //no intersection
    return false;
}

float Triangle::getSurfaceArea() const {
    using namespace Candela::Mathematics;
    
    //these below are cache-able
    const Vector Q1 = vertex_[1] - vertex_[0];
    const Vector Q2 = vertex_[2] - vertex_[0];
    const float Q1Q2 = Q1 * Q2;
    
    return 0.5f * !Q1 * !Q2 * Functions::sqrt(1 - (Q1Q2 * Q1Q2 / (( Q1 * Q1 ) * (Q2 * Q2))));
}


std::string Triangle::toString() const {
    using namespace std;
    ostringstream sstr;
    sstr << "Triangle Shape - Address = " << this
         << "\n\tVertex[0]: " << vertex_[0]
         << "\n\tVertex[1]: " << vertex_[1]
         << "\n\tVertex[2]: " << vertex_[2]
         << "\n\tTexture[0]: " << texture_[0]
         << "\n\tTexture[1]: " << texture_[1]
         << "\n\tTexture[2]: " << texture_[2]
         << "\n\tNormal[0]: " << normal_[0]
         << "\n\tNormal[1]: " << normal_[1]
         << "\n\tNormal[2]: " << normal_[2]
         << "\n\tUnitNormal: " << getUnitNormal({})
    ;
    return sstr.str();
}

Candela::Mathematics::Vector Triangle::getUnitNormal(const Intersection& intersection) const {
    return normal_[0].isNotSet() ?
           ~(( vertex_[2] - vertex_[0] ) ^ ( vertex_[1] - vertex_[0] )) :
           ~(normal_[0] + intersection.uv.x * (normal_[1] - normal_[0]) + intersection.uv.y * (normal_[2] - normal_[0]));
}

void Triangle::setTextureCoordinates(const Mathematics::Vector2<float> &t1, const Mathematics::Vector2<float> &t2,
                                     const Mathematics::Vector2<float> &t3) {
    texture_[0] = t1;
    texture_[1] = t2;
    texture_[2] = t3;
}

Candela::Mathematics::Vector2<float> Triangle::getTextureUv(const Intersection &intersection) const {
    return texture_[0] + intersection.uv.x * (texture_[1] - texture_[0]) + intersection.uv.y * (texture_[2] - texture_[0]);
}

void Triangle::setNormalCoordinates(const Mathematics::Vector &n1, const Mathematics::Vector &n2,
                                    const Mathematics::Vector &n3) {
    normal_[0] = n1;
    normal_[1] = n2;
    normal_[2] = n3;
}

void Triangle::getBounds(AxisAlignedBoundingBox &aabb) const {
    aabb.contain(vertex_[0]);
    aabb.contain(vertex_[1]);
    aabb.contain(vertex_[2]);
}

bool Triangle::partOf(const AxisAlignedBoundingBox &aabb) const {
    // Is any part of this triangle inside the Aabb?
    AxisAlignedBoundingBox triAabb;
    getBounds(triAabb);
    if (triAabb.partOf(aabb))
    {
        Vector vec[2] = {aabb.getMinPosition(), aabb.getMaxPosition()};
        bool behind = false, further = false;
        Vector unitNormal = ~((vertex_[2] - vertex_[0]) ^ (vertex_[1] - vertex_[0]));
        for (int i = 0; i < 8; i++) {
            //P1 and P2 would have worked as well
            if ((unitNormal * (Vector(vec[i & 1].x, vec[(i >> 1) & 1].y, vec[(i >> 2) & 1].z) - vertex_[0])) <= 0) {
                behind |= true;
            } else {
                further |= true;
            }
            if (behind && further) {
                return true;
            }
        }
    }

    return false;
}


