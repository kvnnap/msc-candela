/* 
 * File:   RgbSpectrum.h
 * Author: Kevin
 *
 * Created on 26 April 2014, 13:34
 */

#ifndef RGBSPECTRUM_H
#define	RGBSPECTRUM_H

#include "TriSpectrum.h"

namespace Candela
{
    namespace Spectrum
    {   
        class RgbSpectrum 
                : public TriSpectrum
        {
        public:
            RgbSpectrum();
            RgbSpectrum(float value);
            RgbSpectrum(const RgbSpectrum& rgb);
            RgbSpectrum(const Mathematics::Vector& rgb);
            RgbSpectrum(float red, float green, float blue);

            void alphaCorrect();
            void inverseAlphaCorrect();
            //RgbSpectrum& operator = (const RgbSpectrum& spectrum);

            // Virtual OVerrides
            float getWaveLength(size_t sampleIndex) const override;
            float luminance() const override;
            RgbSpectrum toTristimulusSRGB() const override;
            XyzSpectrum toTristimulusXYZ() const override;

        private:

        };
    }
}

#endif	/* RGBSPECTRUM_H */

