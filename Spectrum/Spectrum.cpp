/* 
 * File:   Spectrum.cpp
 * Author: Kevin
 * 
 * Created on 26 April 2014, 18:46
 */

#include "Spectrum/Spectrum.h"

#include "Spectrum/RgbSpectrum.h"
#include "Spectrum/XyzSpectrum.h"
#include "Mathematics/Float.h"
#include "Mathematics/Funtions.h"
#include "Mathematics/Vector3.h"

#include "Mathematics/Algebra/IFunction.h"
#include "Mathematics/Constants.h"
#include <string>

using namespace Candela::Spectrum;

Spectrum::Spectrum(unsigned short startWaveLength, unsigned char stepSize, 
        const std::vector<float>& samples)
        : startWaveLength_ ( startWaveLength ), stepSize_ ( stepSize ), samples_ ( samples )
{}

float Spectrum::getWaveLength(unsigned sampleIndex) const {
    return startWaveLength_ + stepSize_ * sampleIndex;
}

float Spectrum::luminance() const {
    return toTristimulusXYZ().luminance();
}

RgbSpectrum Spectrum::toTristimulusSRGB() const {
    using namespace Candela::Mathematics;
    return toTristimulusXYZ().toTristimulusSRGB();
}

XyzSpectrum Spectrum::toTristimulusXYZ() const {
    using namespace Candela::Mathematics;
    
    Vector vXyz;
    
    //integrate over the wavelength - convert to trapezoid when time is available
    for(unsigned i = 0; i < samples_.size(); ++i)
    {
        vXyz.x += samples_[i] * Spectrum::X(startWaveLength_ + stepSize_ * i);
        vXyz.y += samples_[i] * Spectrum::Y(startWaveLength_ + stepSize_ * i);
        vXyz.z += samples_[i] * Spectrum::Z(startWaveLength_ + stepSize_ * i);
    }
    
    return XyzSpectrum(vXyz *= stepSize_);
}

//below are anonymous classes
//defining a translation unit variable as static hides it from other translation units
static class X
    : public Candela::Mathematics::Algebra::IFunction<float>
{
    public:
        
        X()
            : reciprocal_1 ( 1.f / 33.33f ), reciprocal_2 ( 1.f / 19.44f )
        {}
    
    private:
        
        float reciprocal_1;
        float reciprocal_2;
        
        virtual float operator ()(float x) const
        {
            using namespace Candela::Mathematics;
            float result;
            float temp = (x - 595.8f) * reciprocal_1;
            result = 1.065f * Functions::pow(Constants::E, -0.5f * temp * temp);
            temp = (x - 446.8f) * reciprocal_2;
            result += 0.366f * Functions::pow(Constants::E, -0.5f * temp * temp);
            return result;
        }

        virtual std::string toString() const
        {
            return "X";
        }
} x;

static class Y
    : public Candela::Mathematics::Algebra::IFunction<float>
{
    public:
        
        Y()
            : lnC1 ( Candela::Mathematics::Functions::ln(556.3f) ), reciprocal ( 1.f / 0.075f )
        {}
            
    private:
        
        float lnC1;
        float reciprocal;

        virtual float operator ()(float x) const
        {
            using namespace Candela::Mathematics;
            x = (Functions::ln(x) - lnC1) * reciprocal;
            return 1.014f * Functions::pow(Constants::E, -0.5f * x * x);
        }
        
        virtual std::string toString() const
        {
            return "Y";
        }

} y;


static class Z
    : public Candela::Mathematics::Algebra::IFunction<float>
{
    public:
        
        Z()
            : lnC1 ( Candela::Mathematics::Functions::ln(449.8f) ), reciprocal ( 1.f / 0.051f )
        {}
            
    private:
        
        float lnC1;
        float reciprocal;
        
        virtual float operator ()(float x) const
        {
            using namespace Candela::Mathematics;
            x = (Functions::ln(x) - lnC1) * reciprocal;
            return 1.839f * Functions::pow(Constants::E, -0.5f * x * x);
        }
        
        virtual std::string toString() const
        {
            return "Z";
        }
} z;

Candela::Mathematics::Algebra::IFunction<float> & Spectrum::X = x;
Candela::Mathematics::Algebra::IFunction<float> & Spectrum::Y = y;
Candela::Mathematics::Algebra::IFunction<float> & Spectrum::Z = z;

void Spectrum::operate(const ISpectrum& spectrum, ISpectrum& result, float(*op)(float, float)) const {
    ((Spectrum&)result).samples_.clear();
    ((Spectrum&)result).samples_.reserve(samples_.size());
    for(unsigned i = 0; i < samples_.size(); ++i)
    {
        ((Spectrum&)result).samples_.push_back(op(samples_[i], ((Spectrum&)spectrum).samples_[i]));
    }
}

void Spectrum::operate(float scale, ISpectrum& result, float(*op)(float, float)) const {
    ((Spectrum&)result).samples_.clear();
    ((Spectrum&)result).samples_.reserve(samples_.size());
    for(unsigned i = 0; i < samples_.size(); ++i)
    {
        ((Spectrum&)result).samples_.push_back(op(samples_[i], scale));
    }
}

void Spectrum::add(const ISpectrum& spectrum, ISpectrum& result) const {
    return operate(spectrum, result, Mathematics::Functions::add);
}

void Spectrum::subtract(const ISpectrum& spectrum, ISpectrum& result) const {
    return operate(spectrum, result, Mathematics::Functions::subtract);
}

void Spectrum::multiply(const ISpectrum& spectrum, ISpectrum& result) const {
    return operate(spectrum, result, Mathematics::Functions::multiply);
}

void Spectrum::multiply(float scale, ISpectrum& result) const {
    return operate(scale, result, Mathematics::Functions::multiply);
}

void Spectrum::divide(const ISpectrum& spectrum, ISpectrum& result) const {
    return operate(spectrum, result, Mathematics::Functions::divide);
}

void Spectrum::divide(float invScale, ISpectrum& result) const {
    return operate(invScale, result, Mathematics::Functions::divide);
}

ISpectrum& Spectrum::operator+=(const ISpectrum& spectrum) {
    operate(spectrum, *this, Mathematics::Functions::add);
    return *this;
}

ISpectrum& Spectrum::operator-=(const ISpectrum& spectrum) {
    operate(spectrum, *this, Mathematics::Functions::subtract);
    return *this;
}

ISpectrum& Spectrum::operator*=(const ISpectrum& spectrum) {
    operate(spectrum, *this, Mathematics::Functions::multiply);
    return *this;
}

ISpectrum& Spectrum::operator*=(float scale) {
    operate(scale, *this, Mathematics::Functions::multiply);
    return *this;
}

ISpectrum& Spectrum::operator/=(const ISpectrum& spectrum) {
    operate(spectrum, *this, Mathematics::Functions::divide);
    return *this;
}

ISpectrum& Spectrum::operator/=(float invScale) {
    operate(invScale, *this, Mathematics::Functions::divide);
    return *this;
}
