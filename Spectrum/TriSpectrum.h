/* 
 * File:   TriSpectrum.h
 * Author: Kevin
 *
 * Created on 17 April 2014, 19:58
 */

#ifndef TRISPECTRUM_H
#define	TRISPECTRUM_H

#include "ISpectrum.h"
#include "Mathematics/Vector3.h"

namespace Candela
{
    namespace Spectrum
    {
        class TriSpectrum
                : public ISpectrum
        {
        public:
            TriSpectrum();
            TriSpectrum(float value);
            TriSpectrum(const TriSpectrum& rgb);
            TriSpectrum(const Mathematics::Vector& rgb);
            TriSpectrum(float a, float b, float c);
            

            void clamp(float min = 0.f, float max = 1.f);
            Mathematics::Vector& getAbcReference();

            // Virtual overrides
            size_t getNumSamples() const override;

            void add(const ISpectrum& spectrum, ISpectrum& result) const override;
            void subtract(const ISpectrum& spectrum, ISpectrum& result) const override;
            void multiply(const ISpectrum& spectrum, ISpectrum& result) const override;
            void multiply(float scale, ISpectrum& result) const override;
            void divide(const ISpectrum& spectrum, ISpectrum& result) const override;
            void divide(float invScale, ISpectrum& result) const override;
            
            ISpectrum& operator += (const ISpectrum& spectrum) override;
            ISpectrum& operator -= (const ISpectrum& spectrum) override;
            ISpectrum& operator *= (const ISpectrum& spectrum) override;
            ISpectrum& operator *= (float scale) override;
            ISpectrum& operator /= (const ISpectrum& spectrum) override;
            ISpectrum& operator /= (float invScale) override;
            
        protected:
            
            Mathematics::Vector abc_;
        };
    }
}


#endif	/* TRISPECTRUM_H */

