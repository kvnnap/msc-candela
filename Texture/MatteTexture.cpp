/* 
 * File:   MatteTexture.cpp
 * Author: Kevin
 * 
 * Created on September 15, 2014, 10:06 PM
 */

#include "Texture/MatteTexture.h"
#include "Spectrum/RgbSpectrum.h"

using namespace std;
using namespace Candela::Texture;
using namespace Candela::Spectrum;

MatteTexture::MatteTexture(std::unique_ptr<Spectrum::ISpectrum> a_spectrum)
    : spectrum ( move(a_spectrum) )
{

}

const Candela::Spectrum::ISpectrum* MatteTexture::GetSpectrum(const Mathematics::Vector2<float>& uv) const {
    return spectrum.get();
}

const MatteTexture MatteTexture::black (unique_ptr<RgbSpectrum>(new RgbSpectrum(1.f)));