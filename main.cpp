/* 
 * File:   main.cpp
 * Author: Kevin
 *
 * Created on 13 December 2013, 20:23
 */

#include <cstdlib>
#include <iostream>
#include "Mathematics/Float.h"
#include "Mathematics/Vector3.h"
#include "Mathematics/BoundedRay.h"
#include "Shape/IBoundingShape.h"
#include "Mathematics/Constants.h"
#include "Shape/Intersection.h"
#include "Shape/Cylinder.h"
#include "Shape/Triangle.h"
#include "Camera/ThinLensCamera.h"
#include "Camera/PinholeCamera.h"
#include "Camera/RealPinholeCamera.h"
#include "Shape/Sphere.h"
#include "Shape/Plane.h"
#include "Shape/AxisAlignedBoundingBox.h"
#include "Mathematics/TMatrix.h"
#include "Mathematics/Matrix.h"
#include "Spectrum/Spectrum.h"
#include "Spectrum/RgbSpectrum.h"
//#include "../src/Mathematics/TMatrix.cpp"
#include "Mathematics/Algebra/DiscreteFunction.h"
#include <stdlib.h>
#include <stdio.h>
#include <memory>
#include "Mesh/LinearMesh.h"
#include <Scene/Scene.h>
#include <Light/PointLight.h>
#include <Material/MatteMaterial.h>
#include <Integrator/SimpleIntegrator.h>
#include <Renderer/SimpleRenderer.h>
#include <Device/FileDevice.h>
#include <Integrator/PathIntegrator.h>
#include <Renderer/ProgressiveRenderer.h>
#include <Scene/SimpleLoader.h>
#include <Scene/WavefrontLoader.h>
#include <Mesh/KdTreeMesh.h>

using namespace std;
using namespace Candela::Camera;
using namespace Candela::Device;
using namespace Candela::Scene;
using namespace Candela::Shape;
using namespace Candela::Mathematics;
using namespace Candela::Light;
using namespace Candela::Spectrum;
using namespace Candela::Mesh;
using namespace Candela::Material;
using namespace Candela::Integrator;
using namespace Candela::Renderer;

bool checkRequirements()
{
    return Float::IEEE754;
}

void constructAndRender() {
    // Scene object
    Scene scene;

    // Construct Camera
//    {
//        unique_ptr<PinholeCamera> camera(new PinholeCamera(
//                Vector::UnitZ,
//                // Spans -4 to 4 and -3 to 3 at a distance of 5
//                CameraPlane(Rectangle_F(8.f, 6.f), 5.f),
//                Rectangle_I(0, 800, 0, 600)
//        ));
//        camera->setCameraPosition(Vector(0, 0.f, -6));
//        //camera->lookAt(Vector(1.f, 0.4f, 0.f));
//        scene.setCamera(move(camera));
//    }

    {
        unique_ptr<ThinLensCamera> camera(new ThinLensCamera(
                Vector::UnitZ,
                // Spans -4 to 4 and -3 to 3 at a distance of 5
                CameraPlane(Rectangle_F(19.2f, 14.4f), 5.f),
                Rectangle_I(0, 800, 0, 600)
        ));
        camera->setCameraPosition(Vector(0, 0.f, -6));
        camera->setFocalLength(6.f);
        //camera->setObjectPlaneDistance(5.f);
        camera->setFilmPlaneDistance(12);
        cout << *camera << endl;
        //camera->lookAt(Vector(1.f, 0.4f, 0.f));
        scene.setCamera(move(camera));
    }

    // Add Light
    scene.addLight(unique_ptr<PointLight>(new PointLight(
            Vector(0.f, 1.8f, -5.f),
            unique_ptr<RgbSpectrum>(new RgbSpectrum(0.9,0.9,0.9)) // albedo
    )));

    // Set mesh
    //scene.setMesh(unique_ptr<IMesh>(new KdTreeMesh(30, 3)));
    scene.setMesh(unique_ptr<IMesh>(new LinearMesh()));

    // Load
    {
        SimpleLoader loader;
        //loader.setFileName("C:\\Users\\kvnna\\ClionProjects\\msc-candela\\build\\CornellBox-Original.obj");
        //loader.setFileName("C:\\Users\\kvnna\\ClionProjects\\msc-candela\\build\\sibenik.obj");
        loader.load(scene);
    }

    // Device
    FileDevice fileDevice;
    fileDevice.setFileName("sphere-simple2");
    //fileDevice.setDirectoryPath("C:\\Users\\kvnna\\Desktop\\renders");
    //fileDevice.setWriteToSequenceOfFiles(true);

    // Integrator
    PathIntegrator integrator;
    integrator.setRayMaxDepth(10);

    ProgressiveRenderer renderer;
    renderer.setIterations(1000);
    renderer.setScene(&scene);
    renderer.setDevice(&fileDevice);
    renderer.setIntegrator(&integrator);

    renderer.render();
}

/*
 * 
 */
int main(int argc, char** argv)
{
    try {
        //Assumptions Check
        if(!checkRequirements())
        {
            cout << "This device cannot properly run this software" << endl;
            return -1;
        }

        constructAndRender();
        return 0;
    } catch(const exception& ex) {
        cout << "Exception: " << ex.what() << endl;
        return -1;
    }
}

